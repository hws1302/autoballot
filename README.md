<img src=img/jcr-logo.png title="Downing JCR Logo" width=100 align="right">


# Downing JCR Room Allocation Program

## The problem

To help tackle COVID, Downing College divided its accommodation into isolated blocks of rooms, in which students would be expected to self isolate together in the event of a lockdown. However, the original room allocations, which were decided in Lent through a ballot process in which students would take turns picking rooms, were not compatible with this. The JCR Committee therefore had to re-run the room ballot, however there was no obvious way to ensure that the groups of students could fit into the room blocks (which varied in size from 1 to 8).

I therefore volunteered to develop a program to automate room swaps between students based on their preferences. This had several advantages over alternative methods:

- Some students would be allowed to remain in their previously allocated rooms;
- Students with medical or religious requirements could 'ballot' with a group of friends (previously they were assigned a room separately);
- Students would not be forced into rooms they could not afford;
- Balloting groups of friends would not be separated, regardless of their size (up to 8 people);
- Fourth year students would be included in the process.

## How it works

The program aimed to maximise overall student happiness by intelligently assigning groups to blocks and students to rooms based on their preferences (among other things). It used the Hungarian algorithm to do so optimally, however this method cannot handle groups / blocks of variable size.

To get around this, the program pseudo-randomly generated over a million possible configurations of blocks (by combining and splitting them where necessary). It then carried forward the configurations in which the block size distribution matches the syndicate size distribution. (This was done in a much more efficient way than this paragraph would suggest, but the result is the same).

This created eight independent optimisation problems (one for each group / block size), and the optimum solution for each was found and evaluated. Slight variations of the top ranked solutions were explored further, until eventually a single solution was chosen.

This solution was chosen by *manually* comparing the anonymised statistics of the top ranked solutions, with the aim of achieving our goals to the fullest extent possible. These goals included things like getting lots of students rooms they can afford and rooms with facilities they like, and as few 'downgrades' from the previous allocations as possible.

> **Much more information about the project and how it works can be found at the links below:**

- [The Notion page](https://www.notion.so/Downing-JCR-Room-Ballot-2020-cbee08722bb94d9aa7824d84087276e5) - everything about the project, how it works and the motivation behind it. The more interesting pages are:
    - [The program (in simple terms)](https://www.notion.so/The-program-in-simple-terms-1c6204690db64c839c651a9a1538e2ab) - a (relatively) short explanation of how it all works;
    - [How we chose the solution](https://www.notion.so/How-we-chose-the-solution-31988b6b7c3f498ba92650ff542cdd23) - some details about how it was run 'on the day';
    - [Solution statistics](https://www.notion.so/Solution-statistics-5543eff2067b4d42b510f53eaea4e187) - lots of graphs showing how the program performed.
- [Full technical documentation](https://m-c-moore.gitlab.io/autoballot/src/index.html) - this was generated using pdoc3, and is a much nicer way to browse the source code.

## Tools used

- [Pandas](https://pandas.pydata.org/) - To load data and store solution statistics.
- [D-Tale](https://github.com/man-group/dtale) - To analyse the solution statistics.
- [Plotly](https://plotly.com/) - To generate the summary graphs.
- [pdoc3](https://pdoc3.github.io/pdoc/) - To generate the [docs page](https://m-c-moore.gitlab.io/autoballot/src/index.html).
