"""File containing the `gen_allocation_page()` function.

(Re-reading the resource files each time is not very efficient, but it is an easy way
to ensure they are not read when pdoc3 is run).
"""

import pandas as pd

from src.models import Room


def _add_table_row(room: Room, room_id: int, band: str, crsid: str, name: str) -> str:
    with open("res/allocations/table_row.html") as f:
        return f.read() % (crsid, room_id, room, crsid, name, band)


def _start_block(block_name: str) -> str:
    with open("res/allocations/block_start.html") as f:
        return f.read() % block_name


def _end_block() -> str:
    with open("res/allocations/block_end.html") as f:
        return f.read()


def gen_allocation_page() -> None:
    """Generate a nicely formatted webpage showing who has been allocated which room."""
    with open("res/allocations/header.html") as f:
        html = f.read()

    cur_block = None
    cur_building = None

    df = pd.read_csv("data/allocations.csv")

    for i, row in df.iterrows():
        if row["block_name"] != cur_block:

            if cur_block is not None:
                html += _end_block()

            if row["building"] != cur_building:
                if cur_building is not None:
                    html += "</div><hr style='background-color:#888; border-width: 3px;'>"

                cur_building = row["building"]
                html += """<div class="row" style="justify-content: center;">"""

            cur_block = row["block_name"]
            html += _start_block(f"{cur_block} ({row['lease']} weeks)")

        html += _add_table_row(
            row["room"], row["room_id"], row["band"], row["crsid"], row["first_name"] + " " + row["last_name"]
        )
    html += _end_block()

    with open("res/allocations/footer.html") as f:
        html += f.read()

    with open("html/allocations.html", "w") as f:
        f.write(html)
