"""File containing the `svae_fig()` function."""

import chart_studio.plotly as p_cs
import plotly.graph_objects as go
import plotly.offline as p_off


def save_fig(fig: go.Figure, file_name: str, auto_open: bool, upload: bool = False) -> None:
    """Save the given figure, and optionally upload it to Chart Studio."""
    file_name = file_name.lower().replace(" ", "_")
    p_off.plot(fig, filename=f"html/{file_name}.html", auto_open=auto_open, include_plotlyjs="directory")

    if upload:
        p_cs.plot(fig, filename=f"ballot-{file_name}.html", auto_open=False)
        print(".", end="")
