"""File containing functions to generate histograms comparing solutions."""

from typing import Optional

import pandas as pd
import plotly.graph_objects as go

from src.results.plot.save_plot import save_fig


def plot_hist(
    column: pd.Series,
    num_bins: int,
    title: str,
    file_name: Optional[str] = None,
    value: Optional[float] = None,
    x_min: Optional[float] = None,
    x_max: Optional[float] = None,
    auto_open: bool = False,
    upload: bool = False,
) -> None:
    """Plot a histogram and colour the bin containing the given `value`."""
    if file_name is None:
        file_name = title

    colours = None if value is None else ["lightslategray"] * num_bins

    x_min = column.min() if x_min is None else x_min
    x_max = column.max() if x_max is None else x_max
    width = (x_max - x_min) / num_bins

    x_lower = x_min

    counts = []
    labels = []

    num_format = "{:.3g}" if x_max < 1000 else "{:.0f}"

    for i in range(num_bins):
        x_upper = x_lower + width

        if value is not None and x_lower <= value < x_upper:
            colours[i] = "crimson"  # type: ignore

        counts.append(((x_lower <= column) & (column < x_upper)).sum())
        labels.append(("[" + num_format + ", " + num_format + ")").format(x_lower, x_upper))

        x_lower = x_upper

    data = [
        go.Bar(
            x=labels,
            y=counts,
            text=labels,
            marker_color=colours,
            name="",
            hovertemplate="<i>Frequency</i>: %{y}<br>%{text}",
        )
    ]

    # add note to title
    want_low = "(want low)" in title
    title = title.replace(" (want low)", "")
    if value is not None:
        rank = ((value > column).sum() if want_low else (value < column).sum()) + 1
        title += (" - <b>" + num_format + "</b>").format(value)
        title += f" (#{rank}/{len(column)})"

    # set useful x label
    x_label = "← Good ← | → Bad →" if want_low else "← Bad ← | → Good →"

    # adjust layout
    layout = go.Layout(
        title=title,
        xaxis_title=x_label,
        yaxis_title="Frequency",
        xaxis=dict(tickvals=[labels[0], labels[-1]], ticktext=[num_format.format(x_min), num_format.format(x_max)]),
        width=650,
        height=450,
        margin=dict(l=20, r=20, t=50, b=20),
    )

    # save figure
    fig = go.Figure(data=data, layout=layout)
    save_fig(fig, file_name, auto_open, upload)


def plot_all() -> None:
    """Read the allocation data and plot all the solution histograms."""
    df = pd.read_csv("data/results/undergrad_stats.csv")

    # extract the chosen solution
    row = df[(df["seed"] == 915278) & (df["exp_seed"] == 259)].iloc[0]

    df["tot_happiness"] = -df["tot_cost"]
    row["tot_happiness"] = -row["tot_cost"]

    hist_columns = {
        "tot_happiness": ("Total happiness", 20),
        "got_upper": ("Asked for and got an upper floor room", 15),
        "got_size": ("Got size within desired range", 15),
        "got_double": ("Asked for and got a double bed", 15),
        "got_new_kitchen": ("Asked for and got a recently renovated kitchen", 15),
        "got_new_room": ("Asked for and got a recently renovated bedroom", 15),
        "got_long_lease": ("Got desired lease length", 15),
        "got_house": ("Asked for and got a house", 15),
        "got_ensuite": ("Asked for and got an ensuite", 15),
        "miss_not_faces_lr": ("Asked for but didn't get a room NOT facing Lensfield (want low)", 13),
        "miss_faces_court": ("Asked for but didn't get a room facing court (want low)", 13),
        "got_staircase": ("Asked for and got a staircase", 15),
        "rank_cost_r": ("Strength of correlation between happiness and ballot rank", 15),
        "price_perc_max": ("Maximum percentage overpay (want low)", 15),
        "price_perc_10": ("Number of students overpaying by >10% (want low)", 15),
        "price_perc_8": ("Number of students overpaying by >8% (want low)", 15),
    }

    for col, item in hist_columns.items():
        plot_hist(df[col], item[1], item[0], col, row[col], auto_open=False, upload=True)
