"""Algorithm parameters and constants definitions.

Notes
-----
The values listed below are the final values that the we used to run the program with.
Each parameter was carefully tuned by Matt and the JCR Committee in an attempt to
produce the fairest solution.

LOOKING AT THESE VALUES WITHOUT UNDERSTANDING HOW THEY ARE BEING USED WILL BE
MISLEADING. For example, `Weights.overpay_mult` is much smaller than
`Vectors.ensuite`. This could imply that we are prioritising people wanting
ensuites over those wanting affordable rooms, but this is far from the truth (have a
look at `ballot/match/cost/price.py` to see why).
"""

from enum import Enum


class Vectors(Enum):
    """Parameters used to generate room facilities and student preference vectors."""

    double = 2.8
    ensuite = 2.6
    upper = 0.4
    faces_lr = 1.2
    faces_court = 0.4
    new_kitchen = 0.6
    new_bathroom = 0.6
    new_room = 1.0
    house = 1.0
    long_lease = 1.5


class Weights(Enum):
    """Parameters used to set the weights of the variables in the match functions.

    Notes
    -----
    The relative sizes of these parameters do not necessarily give any indication of
    how important we felt each variable was. In order to understand that, it is
    necessary to look at how the parameters were used.
    """

    rank = 2.0  # x
    smaller = 2  # x
    larger = 0.8  # x
    size_range = 0.3  # ^

    overpay_offset = 5.0  # x
    overpay_mult = 0.8  # ^

    missed_exp = 2.0  # ^
    missed_mult = 3.0  # x
    missed_a_lot = 0.3  # x


class Constants(Enum):
    """Useful constants used throughout the program."""

    default_rank = 140
    num_in_ballot = 234


class Preferences(Enum):
    """Mapping of form responses to their values."""

    not_at_all = -0.4
    not_really = -0.2
    indifferent = 0.0
    a_little = 0.2
    a_lot = 0.4
    definitely = 0.6

    negative = -0.5
    probably_not = -0.2
    probably = 0.2
    positive = 0.5
