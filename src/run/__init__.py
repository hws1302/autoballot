"""Module containing functions used to run the program.

These will be called manually in a python console as the solutions are being analysed,
so there is no 'single script' that was used to run the program.
"""
