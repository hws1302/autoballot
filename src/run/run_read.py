"""File containing the `run_read()` function."""

from typing import Dict, List, Tuple

import pandas as pd

from src.models import Block, Room, Student, Syndicate
from src.models.utils.to_dataframes import buildings_to_dataframes, syndicates_to_dataframes
from src.readers.read_buildings import read_buildings
from src.readers.read_mock_syndicates import read_mock_syndicates
from src.readers.read_syndicates import read_syndicates


def run_read(
    dir_path: str = "data/parsed_inputs", *, mock: bool = False
) -> Tuple[List[Block], List[Syndicate], List[Room], List[Student], Dict[str, pd.DataFrame], Dict[str, Room]]:
    """Read and return the collection of models and dataframes representing the ballot.

    To run different ballots (e.g. for different years), simply place the input data
    in a different directory.

    Parameters
    ----------
    dir_path
        Path to the directory containing rooms.csv, students.csv and otb.csv.
    mock
        If `True`, will use `read_mock_syndicates()` instead.
    """
    buildings, room_list, pre_allocated = read_buildings(dir_path)
    if mock:
        syndicates, student_list = read_mock_syndicates(pre_allocated, dir_path)
    else:
        syndicates, student_list = read_syndicates(pre_allocated, dir_path)

    print(f"Pre-allocated {len(pre_allocated)} rooms.")

    blocks = [block for building in buildings for block in building.blocks]

    dfs = {}

    dfs["rooms"], dfs["blocks"], dfs["buildings"] = buildings_to_dataframes(buildings)
    dfs["students"], dfs["syndicates"] = syndicates_to_dataframes(syndicates)

    return blocks, syndicates, room_list, student_list, dfs, pre_allocated
