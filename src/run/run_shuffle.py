"""File containing the `run_shuffle()` function."""

import os
from typing import Dict, List, Tuple

import pandas as pd

from src.ballot.evaluate.get_opt_configs import get_opt_configs
from src.ballot.match.StudentMatches import StudentMatches
from src.ballot.match.SyndicateMatches import SyndicateMatches
from src.ballot.shuffle.BlockConfig import BlockConfig
from src.ballot.shuffle.expand import expand_configs
from src.ballot.shuffle.find import find_valid_configs
from src.models import Block, Room, Syndicate


def run_shuffle(
    blocks: List[Block],
    syndicates: List[Syndicate],
    student_matches: StudentMatches,
    syndicate_matches: SyndicateMatches,
    pre_allocated_rooms: Dict[str, Room],
    num_rooms: int,
    num_it: int = 1,
    force_new: bool = False,
    expand: int = 1,
    verbose: bool = True,
) -> Tuple[List[BlockConfig], pd.DataFrame, set]:
    """Run the shuffle stage of the program using the given parameters."""
    syn_sizes = "".join(sorted(str(s.size) for s in syndicates))
    block_sizes = "".join(sorted(str(b.size) for b in blocks))
    file_path = f"data/seeds/{syn_sizes}-{block_sizes}_{num_it}.txt"

    if not force_new and os.path.exists(file_path):
        with open(file_path, "r") as f:
            seeds = [int(line) for line in f.readlines()]
    else:
        seeds = list(range(num_it))

    configs, valid_seeds = find_valid_configs(syndicates, blocks, seeds, verbose)
    if expand > 0:
        configs += expand_configs(configs, expand, verbose)

    with open(file_path, "w+") as f:
        f.writelines(map(lambda x: f"{x}\n", valid_seeds))

    stats_df = get_opt_configs(
        syndicates,
        configs,
        student_matches,
        syndicate_matches,
        pre_allocated_rooms,
        num_rooms,
        detailed=False,
        verbose=verbose,
    )

    print("Valid stage 1 =", stats_df["valid"].sum())
    print("Reasons for failure:\n", stats_df["reason"].unique())

    good_seeds = set(stats_df[stats_df["valid"] == True]["seed"].values)

    return configs, stats_df, good_seeds
