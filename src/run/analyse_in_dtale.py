"""File containing the `analyse_in_dtale()` function."""

import time

import dtale
import pandas as pd
from dtale.views import DtaleData


def analyse_in_dtale(df: pd.DataFrame, save: bool = True) -> DtaleData:
    """Load the given dataframe in dtale, and optionally save it to a timestamped csv."""
    if save:
        time_str = time.strftime("%Y-%m-%d_%H-%M-%S")
        file_name = f"data/saved/{time_str}_({len(df)}).csv"
        df.drop(["config"], axis=1, errors="ignore").to_csv(file_name)

    d = dtale.show(
        df.drop(["indices", "config"], axis=1, errors="ignore"), allow_cell_edits=False, ignore_duplicate=True
    )
    d.open_browser()
    return d
