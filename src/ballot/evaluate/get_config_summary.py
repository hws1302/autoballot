"""File containing the `get_config_summary()` function."""

from typing import Any, DefaultDict, List, TYPE_CHECKING

from src.ballot.evaluate.get_match_stats import get_match_stats

if TYPE_CHECKING:
    from src.ballot.match.SyndicateMatches import SyndicateMatch


def get_config_summary(
    solved_matches: List["SyndicateMatch"], config_stats: DefaultDict[str, Any], temp_stats: DefaultDict[str, Any]
) -> None:
    """Return a dataframe of stats analysing a solution by summarising the allocations.

    Parameters
    ----------
    solved_matches
        List of optimised syndicate matches.

    config_stats
        The current stats dictionary. Modified in place (including the indices).

    temp_stats
        The stats dictionary containing lists. Modified in place.
    """
    for syndicate_match in solved_matches:
        syndicate_match.update_indices(config_stats["indices"])
        for match in syndicate_match.matches:
            get_match_stats(match, config_stats, temp_stats)
