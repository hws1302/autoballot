"""File containing the `get_pre_allocation_summary()` function."""

from typing import Any, DefaultDict, Dict, TYPE_CHECKING

from src.ballot.evaluate.get_match_stats import get_match_stats

if TYPE_CHECKING:
    from src.ballot.match.StudentMatches import StudentMatches
    from src.models import Room, Student


def get_pre_allocation_summary(
    student_matches: "StudentMatches", pre_allocated_rooms: Dict[str, "Room"], config_stats: DefaultDict[str, Any]
) -> None:
    """Return a dataframe of stats analysing the pre-allocations.

    Out the ballot pre-allocations will be ignored - only the students pre-allocated
    because they met the conditions for being able to stay put will be considered.

    Note that these students are not included in the price stats, as in many cases
    their estimated price was very different to the room that they said that they
    wanted to stay in.

    Parameters
    ----------
    student_matches
        `StudentMatches` instance.

    pre_allocated_rooms
        Dictionary of crsids to their pre-allocated room.

    config_stats
        The current stats dictionary. Modified in place.
    """
    for crsid, room in pre_allocated_rooms.items():
        student: "Student" = room.pre_allocated  # type: ignore
        match = student_matches.match(student, room)

        # don't include OTB pre-allocation
        if student.otb_req is not None:
            continue

        # don't include pre-allocation in temp stats
        get_match_stats(match, config_stats, None)
