"""File containing the `analyse_solution()` function."""

from collections import defaultdict
from typing import Any, DefaultDict, Dict, List

import numpy as np
import pandas as pd

from src.ballot.evaluate.utils.anonymise import anonymise_df
from src.ballot.match.StudentMatches import StudentMatches
from src.ballot.validate.check_people_not_together import check_people_not_together
from src.models import Room, Student


def analyse_solution(
    student_list: List[Student],
    room_list: List[Room],
    indices: np.ndarray,
    student_matches: StudentMatches,
    anon: bool = False,
) -> pd.DataFrame:
    """Return a dataframe of stats analysing a solution on a student-by-student basis.

    Parameters
    ----------
    student_list
        List of students. Must be in the original order.

    room_list
        List of rooms. Must be in the original order.

    indices
        Array of indices, where the index is the room index and the value is the
        index of the student assigned to that room.

    student_matches
        `StudentMatches` instance.

    anon
        If set `True`, will anonymise the dataframe (see `anonymise_df()`).
    """
    stats: DefaultDict[int, Dict[str, Any]] = defaultdict()

    for i, r in enumerate(room_list):
        if r.pre_allocated is not None:
            s = r.pre_allocated
        else:
            index = int(indices[r.index])
            if index == -1:
                continue

            s = student_list[index]

        match = student_matches.match(s, r)

        stats[i] = dict(
            cost=match.cost,
            crsid=s.crsid,
            room=r.id,
            building=r.building.id,
            block=r.block.id,
            rank=s.rank,
            syn_id=None if s.syndicate is None else s.syndicate.id,  # noqa
            syn_size=None if s.syndicate is None else s.syndicate.size,  # noqa
            wants_double=s.wants_double,
            is_double=int(r.is_double),
            wants_ensuite=s.wants_ensuite,
            is_ensuite=int(r.is_ensuite),
            wants_upper=s.wants_upper,
            is_upper=int(r.is_upper),
            wants_faces_court=s.wants_faces_court,
            faces_court=int(r.faces_court),
            wants_not_faces_lr=s.wants_not_faces_lr,
            not_faces_lr=int(r.not_faces_lr),
            wants_min_size=s.wants_min_size,
            wants_max_size=s.wants_max_size,
            size=r.size,
            wants_new_kitchen=s.wants_new_kitchen,
            new_kitchen=r.new_kitchen,
            wants_new_bathroom=s.wants_new_bathroom,
            new_bathroom=r.new_bathroom,
            wants_new_room=s.wants_new_room,
            new_room=r.new_room,
            est_price=s.est_price,
            room_price=r.price,
            perc_price=100 * (r.price - s.est_price) / s.est_price,
            is_split=int(r.block.is_split),
            is_combined=int(r.block.is_combined),
            otb=s.otb_req,
            pre_allocated=r.pre_allocated is not None,
        )

    df = pd.DataFrame.from_dict(stats, orient="index")

    if anon:
        anonymise_df(df)
    else:
        check_people_not_together(df)

    return df.set_index("crsid")
