"""File containing the `get_opt_configs()` function."""

import time
from collections import defaultdict
from typing import Any, DefaultDict, Dict, List, TYPE_CHECKING

import numpy as np
import pandas as pd
import scipy.stats as sp_stats
from tqdm import tqdm

from src.ballot.evaluate.get_config_summary import get_config_summary
from src.ballot.evaluate.get_pre_allocation_summary import get_pre_allocation_summary
from src.ballot.match.solve_assignment import solve_syndicate_assignment

if TYPE_CHECKING:
    from src.ballot.match.StudentMatches import StudentMatches
    from src.ballot.match.SyndicateMatches import SyndicateMatch, SyndicateMatches
    from src.ballot.shuffle.BlockConfig import BlockConfig
    from src.models import Room, Syndicate


def get_opt_configs(
    syndicates: List["Syndicate"],
    valid_configs: List["BlockConfig"],
    student_matches: "StudentMatches",
    syndicate_matches: "SyndicateMatches",
    pre_allocated_rooms: Dict[str, "Room"],
    num_rooms: int,
    detailed: bool = True,
    verbose: bool = True,
) -> pd.DataFrame:
    """Return a dictionary of statistics summarising the solution for each config.

    This function optimally assigns syndicates to blocks for each configuration, and
    generates some statistics about each configuration's optimum to allow them to be
    compared.

    Parameters
    ----------
    syndicates
        List of syndicates.

    valid_configs
        List of valid configs (i.e. syndicate sizes match block sizes).

    student_matches
        `StudentMatches` instance.

    syndicate_matches
        `SyndicateMatches` instance.

    pre_allocated_rooms
        Dictionary of crsids to their pre-allocated room.

    num_rooms
        Number of rooms. Could be calculated, but is passed in for efficiency.

    detailed
        If `True`, calculates a detailed set of statistics.

    verbose
        If `True`, prints messages as it runs.
    """
    start = time.time()

    if verbose:
        print(f"\nOptimising and evaluating {len(valid_configs)} configurations:")

    stats: DefaultDict[int, DefaultDict[str, Any]] = defaultdict(lambda: defaultdict(int))

    # get syndicates by size
    syndicates_by_size: Dict[int, List["Syndicate"]] = defaultdict(lambda: [])
    for syndicate in syndicates:
        syndicates_by_size[syndicate.size].append(syndicate)

    for i, config in enumerate(tqdm(valid_configs)):
        # initialise stats
        stats[i]["seed"] = config.seed
        stats[i]["exp_seed"] = config.exp_seed
        stats[i]["splits"] = len(config.split_blocks)
        stats[i]["valid"] = True
        stats[i]["reason"] = None
        stats[i]["config"] = config
        stats[i]["indices"] = 0 - np.ones(num_rooms, dtype=np.int)

        temp_stats: DefaultDict[str, List] = defaultdict(lambda: [])

        # iterate through syndicate / block sizes, and optimally solve the assignment
        # problem for each. Will break loop if one size is unsolvable
        for size in syndicates_by_size.keys():
            cost, res = solve_syndicate_assignment(
                syndicates_by_size[size], config.blocks_by_size[size], syndicate_matches
            )

            stats[i]["tot_cost"] += cost
            if cost == np.inf:
                stats[i]["valid"] = False
                stats[i]["reason"] = tuple(sorted(res))
                break

            solved_matches: List["SyndicateMatch"] = res  # type: ignore
            if detailed:
                get_config_summary(solved_matches, stats[i], temp_stats)
            else:
                for syndicate_match in solved_matches:
                    syndicate_match.update_indices(stats[i]["indices"])

        if detailed:
            get_pre_allocation_summary(student_matches, pre_allocated_rooms, stats[i])

            # calculate summary stats from temp_stats
            cost_arr = np.array(temp_stats["cost"], dtype=np.float)
            stats[i]["cost_var"] = np.var(cost_arr)
            stats[i]["cost_max"] = np.max(cost_arr)

            rank_arr = np.array(temp_stats["rank"], dtype=np.float)

            try:
                # note we use Spearman not Pearson as rank is uniformly distributed
                stats[i]["rank_cost_r"], stats[i]["rank_cost_p"] = sp_stats.spearmanr(
                    cost_arr, rank_arr, nan_policy="omit"  # noqa
                )
            except ValueError:
                # Fails for students without ranks <3 entries.
                pass

            price_perc_arr = np.array(temp_stats["price_perc"], dtype=np.float)
            stats[i]["price_perc_max"] = np.max(price_perc_arr)
            stats[i]["price_perc_10"] = np.sum(price_perc_arr > 10)
            stats[i]["price_perc_8"] = np.sum(price_perc_arr > 8)

    if verbose:
        print(f"\tFinished in {time.time() - start:.3f} s.")

    return pd.DataFrame.from_dict(stats, orient="index")
