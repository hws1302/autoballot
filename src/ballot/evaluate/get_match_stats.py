"""File containing the `get_match_stats()` function."""

from typing import Any, DefaultDict, Optional, TYPE_CHECKING

from src.params import Preferences

if TYPE_CHECKING:
    from src.ballot.match.StudentMatches import StudentMatch


def get_match_stats(
    match: "StudentMatch", config_stats: DefaultDict[str, Any], temp_stats: Optional[DefaultDict[str, Any]] = None
) -> None:
    """Update the stats dictionary with the values of a single room allocation.

    Parameters
    ----------
    match
        A single student-room match

    config_stats
        The current stats dictionary. Modified in place.

    temp_stats
        The stats dictionary containing lists. Modified in place.
    """
    s = match.student
    r = match.room

    # threshold value for considered to 'want' something
    threshold = Preferences.a_lot.value - 0.001  # subtract for float errors

    if s.wants_double > threshold and r.is_double:
        config_stats["got_double"] += 1

    if s.wants_ensuite > threshold and r.is_ensuite:
        config_stats["got_ensuite"] += 1

    if s.wants_upper > threshold and r.is_upper:
        config_stats["got_upper"] += 1

    if s.wants_faces_court > threshold and not r.faces_court:
        config_stats["miss_faces_court"] += 1

    if s.wants_not_faces_lr > threshold and not r.not_faces_lr:
        config_stats["miss_not_faces_lr"] += 1

    if s.wants_new_kitchen > threshold and r.new_kitchen > 0.75:
        config_stats["got_new_kitchen"] += 1

    if s.wants_new_bathroom > threshold and r.new_bathroom > 0.75:
        config_stats["got_new_room"] += 1

    if s.wants_new_room > threshold and r.new_room > 0.75:
        config_stats["got_new_room"] += 1

    if s.wants_min_size <= r.size <= s.wants_max_size:
        config_stats["got_size"] += 1

    if s.wants_long_lease == Preferences.positive.value and r.long_lease:
        config_stats["got_long_lease"] += 1

    if s.wants_long_lease == Preferences.negative.value and not r.long_lease:
        config_stats["got_short_lease"] += 1

    if s.wants_house == Preferences.positive.value and r.is_house:
        config_stats["got_house"] += 1

    if s.wants_house == Preferences.negative.value and not r.is_house:
        config_stats["got_staircase"] += 1

    if temp_stats is None:
        return

    temp_stats["cost"].append(match.cost)
    temp_stats["rank"].append(s.rank)

    price_perc = (r.price - s.est_price) / s.est_price * 100
    temp_stats["price_perc"].append(price_perc)
