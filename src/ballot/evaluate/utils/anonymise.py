"""File containing the `get_animals()` and `anonymise_df()` functions."""

from typing import List

import pandas as pd


def get_animals() -> List[str]:
    """Return a list of 488 animal names."""
    animals = []
    with open("res/animals.txt", "r") as f:
        animal = f.readline()
        while animal:
            animals.append(animal.strip())
            animal = f.readline()
    return animals


def anonymise_df(df: pd.DataFrame) -> None:
    """Anonymise a dataframe in place by replacing the crsids with animal names."""
    df["crsid"] = get_animals()[: len(df)]
