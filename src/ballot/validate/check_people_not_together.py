"""File containing the `check_people_not_together()` function."""

import pandas as pd


def check_people_not_together(solution_df: pd.DataFrame) -> None:
    """Print warning messages if people live together who can't."""
    df = pd.read_csv("data/not_together.csv")

    valid = True

    for _, row in df.iterrows():
        crsid_1 = row["crsid_1"]
        crsid_2 = row["crsid_2"]

        building_1 = solution_df["building"][crsid_1]
        building_2 = solution_df["building"][crsid_2]

        if building_1 == building_2:
            valid = False
            if row["extent"] == "building":
                print(f"ERROR: {crsid_1} is in the same building as {crsid_2}")
            else:
                print(f"WARN: {crsid_1} may be in the same block as {crsid_2}")

    if valid:
        print("No-one is in the same building as someone they cannot be with.")
