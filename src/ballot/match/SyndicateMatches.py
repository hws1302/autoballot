"""File containing the `SyndicateMatch` and `SyndicateMatches` classes."""

from __future__ import annotations

from typing import List, TYPE_CHECKING

import numpy as np

from src.ballot.match.Matches import Match, Matches
from src.ballot.match.solve_assignment import solve_student_assignment

if TYPE_CHECKING:
    from src.ballot.match.StudentMatches import StudentMatch, StudentMatches
    from src.models import Block, Syndicate


class SyndicateMatch(Match):
    """Class representing a syndicate-block match. Used to calculate the match cost."""

    def __init__(self, match_id: str, syndicate: "Syndicate", block: "Block", student_matches: "StudentMatches"):
        """Initialise a `SyndicateMatch` instance. Calculates the cost immediately."""
        assert block.size >= syndicate.size, "Block must be larger than syndicate"
        self.matches: List["StudentMatch"] = []
        self._student_matches = student_matches

        super().__init__(match_id, syndicate, block)

    @property
    def syndicate(self) -> "Syndicate":
        return self._source

    @property
    def block(self) -> "Block":
        return self._dest

    def _calculate(self) -> None:
        """Assign students to rooms and calculate the cost of the match."""
        # optimally assign students to rooms, and calculate the cost
        self.cost, res = solve_student_assignment(self.syndicate, self.block, self._student_matches)

        # handle invalid assignment. Note we still loop through all the matches
        if self.cost == np.inf:
            self.valid = False
            self.reasons = res  # type: ignore
        else:
            self.matches = res  # type: ignore

    def update_indices(self, indices: np.ndarray) -> None:
        """Update the student-room indices array with the syndicate's allocations."""
        assert len(self.matches) > 0

        for match in self.matches:
            assert indices[match.room.index] == -1
            indices[match.room.index] = match.student.index


class SyndicateMatches(Matches):
    """Class to manage a collection of `SyndicateMatch` instances."""

    def __init__(self, student_matches: "StudentMatches"):
        """Initialise a `SyndicateMatches` instance."""
        super().__init__(SyndicateMatch, student_matches)
