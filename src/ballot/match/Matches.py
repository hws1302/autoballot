"""File containing the `Match` and `Matches` abstract base classes."""

from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Any, Dict, Generic, Optional, Type, TypeVar, Union

from src.models import Block, Room, Student, Syndicate

TSource = TypeVar("TSource", Student, Syndicate)
TDest = TypeVar("TDest", Room, Block)
TMatch = TypeVar("TMatch", bound="Match")


class Match(ABC, Generic[TSource, TDest]):
    """Abstract base class from which `StudentMatch` and `SyndicateMatch` are extended."""

    def __init__(self, match_id: str, source: TSource, dest: TDest):
        """Initialise a `Match` instance. Calculates the cost immediately."""
        self.id = match_id
        self._source = source  # type: ignore
        self._dest = dest  # type: ignore

        # cost of the match
        self.cost = 0.0  # cost of the match
        self.valid = True  # set to false if not a valid match (i.e. requirements not met)
        self.reason: Optional[str] = None  # reason for invalid match (None if valid)

        # calculate the match parameters
        self._calculate()

    def __repr__(self) -> str:
        return f"{self.id} = {self.cost:.2f}"

    @abstractmethod
    def _calculate(self) -> None:
        """Calculate the cost of the match."""
        pass


class Matches(ABC, Generic[TMatch]):
    """Abstract base class from which `StudentMatches` and `SyndicateMatches` are extended.

    It is used to store a map of `Match` subclass instances to avoid recalculating as
    this tends to be computationally expensive.
    """

    def __init__(self, match_cls: Type[TMatch], *match_cls_args: Any):
        """Initialise a `Matches` instance. This should be done once.

        Parameters
        ----------
        match_cls
            `Match` subclass, instances of which will be returned by `Matches.match()`.
        match_cls_args
            Arguments to be passed to `match_cls.__init__()` in addition to `match_id`,
            `source` and `dest`.
        """
        self._matches: Dict[str, TMatch] = {}
        self._match_cls = match_cls
        self._match_cls_args = match_cls_args

    def __repr__(self) -> str:
        return "\n".join(str(match) for match in self._matches.values())

    def __getitem__(self, item: str) -> TMatch:
        return self._matches[item]

    def __setitem__(self, key: str, value: TMatch) -> None:
        assert key not in self._matches, "Duplicate match"
        self._matches[key] = value

    def match(self, source: Union[Student, Syndicate], dest: Union[Room, Block]) -> TMatch:
        """Return the `Match` subclass instance for the given `source` and `dest`.

        If this match has already been made, return the original match. Otherwise
        initialise a new one and store it.
        """
        match_id = f"{source.id} -> {dest.id}"

        try:
            # try to return previously calculated match
            return self[match_id]

        except KeyError:
            # if it does not exist, save a new one and return it
            match = self._match_cls(match_id, source, dest, *self._match_cls_args)
            self[match_id] = match
            return match
