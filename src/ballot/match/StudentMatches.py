"""File containing the `StudentMatch` and `StudentMatches` classes."""

from __future__ import annotations

from typing import TYPE_CHECKING

import numpy as np

from src.ballot.match.costs.features import calc_feature_match_cost
from src.ballot.match.costs.price import calc_est_price_cost
from src.ballot.match.costs.size import calc_size_cost
from src.ballot.match.Matches import Match, Matches
from src.params import Constants, Weights

if TYPE_CHECKING:
    from src.models import Room, Student


class StudentMatch(Match):
    """Class representing a student-room match. Used to calculate the match cost."""

    def __init__(self, match_id: str, student: "Student", room: "Room"):
        """Initialise a `StudentMatch` instance. Calculates the cost immediately."""
        self.feature_cost = 0.0
        self.price_cost = 0.0
        self.size_cost = 0.0

        super().__init__(match_id, student, room)

    @property
    def student(self) -> "Student":
        return self._source

    @property
    def room(self) -> "Room":
        return self._dest

    def _calculate(self) -> None:
        """Calculate the cost of the match."""
        # out of ballot needs not met -> infinite cost
        if self.student.otb_req is not None and self.student.otb_req.is_missing_requirements(self.room):
            self.cost = np.inf
            self.valid = False
            self.reason = "OTB - " + self.student.crsid
            return

        # calculate costs
        self.feature_cost = calc_feature_match_cost(self.student, self.room)
        self.size_cost = calc_size_cost(self.student, self.room)
        self.price_cost = calc_est_price_cost(self.student, self.room)

        # scale with ballot rank, mapped to number between 1 and (1 + Weights.rank)
        rank = Constants.default_rank.value if self.student.rank is None else self.student.rank
        n = Constants.num_in_ballot.value
        self.cost = (self.feature_cost + self.size_cost + self.price_cost) * (1 + Weights.rank.value * (n - rank) / n)


class StudentMatches(Matches):
    """Class to manage a collection of `StudentMatch` instances."""

    def __init__(self) -> None:
        """Initialise a `StudentMatches` instance."""
        super().__init__(StudentMatch)
