"""File containing the `calc_feature_match_cost()` function."""

from typing import TYPE_CHECKING

from src.params import Preferences, Weights

if TYPE_CHECKING:
    from src.models import Room, Student


def calc_feature_match_cost(student: "Student", room: "Room") -> float:
    """Return the cost of assigning a student to a room based on their preferences.

    It considers the alignment of the preferences vector, but also penalises students
    missing multiple features that they wanted. To do this, the fraction of missing
    features is considered, which effectively means that students with few strong
    preferences are more likely to have them met.
    """
    # alignment of preferences
    cost = -student.vec @ room.vec

    # penalise missing preferences (considering fraction)
    def_prefs = student.vec == Preferences.definitely.value
    a_lot_prefs = student.vec == Preferences.a_lot.value
    missing_features = room.vec < 0

    # need max(1, ...) if sum(...) == 0
    frac_def = sum(def_prefs & missing_features) / max(1.0, sum(def_prefs))
    frac_a_lot = sum(a_lot_prefs & missing_features) / max(1.0, sum(a_lot_prefs))

    eff_frac = 1 + frac_def + frac_a_lot * Weights.missed_a_lot.value
    cost += Weights.missed_mult.value * eff_frac ** Weights.missed_exp.value

    return cost
