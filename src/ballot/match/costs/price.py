"""File containing the `calc_est_price_cost()` function."""

from typing import TYPE_CHECKING

import numpy as np

from src.params import Preferences, Weights

if TYPE_CHECKING:
    from src.models import Room, Student


def calc_est_price_cost(student: "Student", room: "Room") -> float:
    """Return the cost of assigning a student to a room based on their price preference.

    This is essentially an exponential penalty function.

    Notes
    -----
    For students who specifically requested short lease rooms, the total room price
    over the year will be considered (i.e. the weekly room price will be considered to
    be 37/30 = 1.23 times greater). For students who said they "probably" want a short
    lease room, the scaling factor is 33/30.
    """
    # scale price if wanted short lease room
    scaled_price: float = room.price

    if room.long_lease:
        if student.wants_long_lease == Preferences.negative.value:
            scaled_price *= 37.0 / 30.0
        elif student.wants_long_lease == Preferences.probably_not.value:
            scaled_price *= 33.0 / 30.0

    # calculate cost
    perc_diff = (scaled_price - student.est_price) / student.est_price * 100.0
    return Weights.overpay_offset.value * np.exp(Weights.overpay_mult.value * (perc_diff - student.price_perc_over))
