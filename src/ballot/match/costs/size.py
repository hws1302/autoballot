"""File containing the `calc_size_cost()` function."""

from typing import TYPE_CHECKING

from src.params import Weights

if TYPE_CHECKING:
    from src.models import Room, Student


def calc_size_cost(student: "Student", room: "Room") -> float:
    """Return the cost of assigning a student to a room based on their size preference.

    Notes
    -----
    Narrow ranges are weighted less strongly than wide ranges.
    """
    size_range_factor = ((student.wants_max_size - student.wants_min_size + 1) / 31) ** Weights.size_range.value

    if room.size < student.wants_min_size:
        return (student.wants_min_size - room.size) * Weights.smaller.value * size_range_factor

    if room.size > student.wants_max_size:
        return (room.size - student.wants_max_size) * Weights.larger.value * size_range_factor

    return 0.0
