"""Module containing functions and classes to calculate allocation costs.

This module handles the calculations for both student-room and syndicate-block
allocations. In each case, once a match has been made and the associated cost
calculated, it will be stored and reused to avoid repeating the same calculations.
"""
