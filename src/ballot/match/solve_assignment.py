"""File containing functions to optimally solve room / block assignments."""

from typing import Dict, List, Optional, Set, Tuple, TYPE_CHECKING, Union

import numpy as np
from lapsolver import solve_dense

if TYPE_CHECKING:
    from src.ballot.match.StudentMatches import StudentMatch, StudentMatches
    from src.ballot.match.SyndicateMatches import SyndicateMatch, SyndicateMatches
    from src.models import Block, Syndicate


def _solve_assignment(cost_matrix: np.ndarray) -> Tuple[Optional[np.ndarray], np.ndarray]:
    """Return the row and column indices of the optimal assignments.

    Return (`None`, []) if infeasible.

    Parameters
    ----------
    cost_matrix
        Cost matrix to solve. Must be wide.
    """
    assert len(cost_matrix) <= len(cost_matrix[0]), "Matrix must be wide"

    rows, columns = solve_dense(cost_matrix)

    if len(rows) != len(cost_matrix):
        return None, np.empty(0)

    return rows, columns


def solve_student_assignment(
    syndicate: "Syndicate", block: "Block", student_matches: "StudentMatches"
) -> Tuple[float, Union[List["StudentMatch"], Set[str]]]:
    """Solve the student-room assignment problem within a syndicate / block.

    The first return value is the cost of the match. The second is a list of student
    matches if the algorithm was successful, otherwise it is a set of reasons for why
    it failed.
    """
    cost_matrix = np.zeros((syndicate.size, block.size))
    reasons: Set[str] = set()

    for i, student in enumerate(syndicate.students):
        all_inf = True
        reasons.clear()

        for j, room in enumerate(block.rooms):
            match = student_matches.match(student, room)
            cost_matrix[i, j] = match.cost

            if match.cost != np.inf:
                all_inf = False
            else:
                reasons.add(match.reason)  # type: ignore

        if all_inf:
            return np.inf, reasons

    # calculate optimal assignment
    rows, cols = _solve_assignment(cost_matrix)

    # if infeasible return infinite cost. May occur in special case where row is not
    # full of infinities, but is still impossible
    if rows is None:
        return np.inf, {"Students", *reasons}

    # build results
    cost = 0.0
    matches = []

    for i in range(len(rows)):
        student = syndicate.students[rows[i]]
        room = block.rooms[cols[i]]

        # TODO don't re-read match here
        match = student_matches.match(student, room)

        matches.append(match)
        cost += match.cost

    return cost, matches


_ssa_cache: Dict[int, Tuple[float, Union[List["SyndicateMatch"], Set[str]]]] = {}


def _save_and_return(
    key: int, cost: float, result: Union[List["SyndicateMatch"], Set[str]]
) -> Tuple[float, Union[List["SyndicateMatch"], Set[str]]]:
    _ssa_cache[key] = (cost, result)
    return cost, result


def solve_syndicate_assignment(
    syndicates: List["Syndicate"], blocks: List["Block"], syndicate_matches: "SyndicateMatches"
) -> Tuple[float, Union[List["SyndicateMatch"], Set[str]]]:
    """Solve the syndicate-block assignment problem.

    The first return value is the cost of the match. The second is a list of syndicate
    matches if the algorithm was successful, otherwise it is a set of reasons for why
    it failed.
    """
    # key for memoization
    key = hash(frozenset(s.id for s in syndicates).union(frozenset(b.id for b in blocks)))

    try:
        return _ssa_cache[key]
    except KeyError:
        pass

    cost_matrix = np.zeros((len(syndicates), len(blocks)))
    reasons: Set[str] = set()

    for i, syndicate in enumerate(syndicates):
        all_inf = True
        reasons.clear()

        for j, block in enumerate(blocks):
            match = syndicate_matches.match(syndicate, block)
            cost_matrix[i, j] = match.cost
            if match.cost != np.inf:
                all_inf = False
            else:
                reasons.update(match.reasons)  # type: ignore
        if all_inf:
            return _save_and_return(key, np.inf, reasons)

    # calculate optimal assignment
    rows, cols = _solve_assignment(cost_matrix)

    # if infeasible return infinite cost. May occur in special case where row is not
    # full of infinities, but is still impossible
    if rows is None:
        return _save_and_return(key, np.inf, {"Syndicates", *reasons})

    # build results
    cost = 0.0
    matches = []

    for i in range(len(rows)):
        syndicate = syndicates[rows[i]]
        block = blocks[cols[i]]

        # TODO don't re-read match here
        match = syndicate_matches.match(syndicate, block)

        matches.append(match)
        cost += match.cost

    return _save_and_return(key, cost, matches)
