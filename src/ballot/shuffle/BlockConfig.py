"""File containing the `BlockConfig` and `BlockConfigGenerator` classes."""

import random
from collections import defaultdict
from dataclasses import dataclass, field
from typing import DefaultDict, Dict, List, Optional, Set, TYPE_CHECKING, Union

import numpy as np

from src.ballot.shuffle.BlockSplitter import BlockSplitter

if TYPE_CHECKING:
    from src.models import Block, SCBlock, SBlock

_MAX_GAP = 2


@dataclass(init=True, repr=True, eq=False, order=False, frozen=True)
class BlockConfig:
    """Class representing a single valid block configuration."""

    seed: int
    exp_seed: Optional[int]
    blocks_by_size: Dict[int, List["Block"]] = field(repr=False)
    split_blocks: Dict[str, List[Union["SBlock", "SCBlock"]]] = field(repr=False)


class BlockConfigGenerator:
    """Class used to generate a block configuration using a random seed."""

    def __init__(
        self,
        seed: int,
        blocks: List["Block"],
        syndicate_sizes: np.ndarray,
        block_splitter: BlockSplitter,
        gaps_allowed: int,
        sort_desc: bool = False,
    ):
        """Initialise a block configuration generator.

        This shuffles a shallow copy of the blocks list using the given random seed.
        This means that the same random configuration will be created if the same
        parameters are used

        Parameters
        ----------
        seed
            Number used to seed `random.shuffle()`.

        blocks
            List of blocks

        syndicate_sizes
            Array of syndicate sizes, with the number of a certain syndicate size
            being given by `syndicate_sizes[size - 1]`.

        gaps_allowed
            Number of empty rooms allowed (i.e. count of rooms minus count of students).
            Although this can be calculated from the other inputs, it should be passed
            in here for efficiency.

        sort_desc
            If True, sorts blocks by size descending after shuffling. This greatly
            increases the percentage of valid configurations, but does decrease the
            randomness.
        """
        self._blocks = blocks.copy()  # copy as will be shuffled
        self._syn_size_left = syndicate_sizes.copy()
        self._gaps_remaining = gaps_allowed
        self._max_syn_size = np.max(np.nonzero(self._syn_size_left)) + 1  # maximum syndicate size

        self._block_splitter = block_splitter
        self._block_splitter.reset()

        # initialise config variables
        self._used_block_ids: Set[str] = set()
        self.blocks_by_size: Dict[int, List["Block"]] = defaultdict(lambda: [])

        # dictionary of block ids to its split blocks
        self.split_blocks: DefaultDict[str, List[Union["SBlock", "SCBlock"]]] = defaultdict(lambda: [])

        # seed the random module, and shuffle a copy of the blocks list
        self.seed = seed
        random.seed(self.seed)
        random.shuffle(self._blocks)

        if sort_desc:
            self._blocks.sort(key=lambda b: b.size, reverse=True)

    def _use_block(self, block: "Block", block_id: str, eff_size: int) -> bool:
        gap_size = max(0, eff_size - self._max_syn_size)  # probably zero
        if gap_size > max(_MAX_GAP, self._gaps_remaining):
            return False
        eff_size -= gap_size

        # use the block if it is a size we need
        if self._syn_size_left[eff_size - 1] > 0:
            self._syn_size_left[eff_size - 1] -= 1
            self._used_block_ids.add(block_id)
            self.blocks_by_size[eff_size].append(block)

            self._gaps_remaining -= gap_size
            return True

        return False

    def _use_if_useful(self, index: int) -> bool:
        block = self._blocks[index]
        return self._use_block(block, block.id, block.size)

    def _combine_if_useful(self, index: int) -> bool:
        block = self._blocks[index]

        # look at the blocks in the same building that haven't been used
        # note this assumes that block.id is already in _used_block_ids
        remaining_blocks = [b for b in block.building.blocks if b.id not in self._used_block_ids]

        # attempt to combine with one other block.
        for r_block in remaining_blocks:
            # skip pre-allocated blocks
            if r_block.size == 0:
                continue

            # use if combined size is one that we need
            combined_block = block.combine(r_block)
            comb_size = combined_block.size
            if self._use_block(combined_block, r_block.id, comb_size):
                return True

            # allow combined block be split with a syndicate of 1. We don't want
            # to perform any more complicated combine-and-splits as the students will
            # be "randomly" scattered throughout the effective block.
            gap_size = max(0, comb_size - (self._max_syn_size + 1))
            if gap_size > max(_MAX_GAP, self._gaps_remaining):
                continue

            comb_size -= gap_size

            if self._syn_size_left[comb_size - 2] > 0 and self._syn_size_left[0] > 0:
                new_combined, single = block.combine_and_split(r_block, block.rooms[0])
                self._used_block_ids.add(r_block.id)
                new_comb_size = new_combined.size - gap_size

                self.blocks_by_size[new_comb_size].append(new_combined)
                self.blocks_by_size[1].append(single)

                self.split_blocks[block.id].append(new_combined)
                self.split_blocks[block.id].append(single)

                self._syn_size_left[new_comb_size - 1] -= 1
                self._syn_size_left[0] -= 1

                self._gaps_remaining -= gap_size
                return True

            # TODO may need to combine >2 blocks if have lots of large syndicates

        return False

    def _split_if_useful(self, index: int) -> bool:
        block = self._blocks[index]
        last_split_invalid = False

        while True:
            split, diff = self._block_splitter.get_split(block.size, last_split_invalid)

            # TODO allow gaps
            if split is None:
                return False

            # check useful split
            if all(self._syn_size_left - diff >= 0):
                self._syn_size_left -= diff

                # we don't shuffle the rooms, as will do so when we 'expand' it later
                index = 0
                rooms_lists = []
                for split_size in split:
                    rooms_lists.append(block.rooms[index : split_size + index])
                    index += split_size

                # TODO loop through once
                for split_block in block.split(rooms_lists):
                    self.blocks_by_size[split_block.size].append(split_block)
                    self.split_blocks[block.id].append(split_block)

                return True

            last_split_invalid = True

    def _done(self) -> bool:
        return np.all(self._syn_size_left == 0)

    def is_valid(self) -> bool:
        """Return `True` if a valid configuration has been generated.

        This function iterates over the shuffled list of blocks, and will extract,
        combine or split each block as necessary.
        """
        # iterate through the shuffled blocks
        for i, block in enumerate(self._blocks):
            # skip blocks already used (as combined)
            if block.id in self._used_block_ids:
                continue

            self._used_block_ids.add(block.id)
            size = block.size

            # skip blocks of size 0 (i.e. fully pre-allocated)
            # note we do this after adding too used, to keep indices correct
            if size == 0:
                continue

            # add immediately if need one of that size
            if self._use_if_useful(i):
                continue

            # combine block if useful. We attempt this first because it is preferable to
            # splitting blocks, which leads to different syndicates living together
            if sum(self._syn_size_left[size:]) >= 1:
                if self._combine_if_useful(i):
                    continue

            # split the block into smaller ones if useful
            if sum(self._syn_size_left[: size - 1]) >= 2:
                if self._split_if_useful(i):
                    continue

            # block was not used, so remove from set
            self._used_block_ids.remove(block.id)
            self._gaps_remaining -= size

            if self._gaps_remaining < 0:
                return False

            if self._done():
                return True

        return self._done()

    def build(self) -> BlockConfig:
        """Return a `BlockConfig` instance using this generator's attributes."""
        return BlockConfig(self.seed, None, self.blocks_by_size, self.split_blocks)
