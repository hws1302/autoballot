"""File containing the `BlockSplitter` class."""

from typing import List, Tuple, Union

import numpy as np


def _find_combinations_recursive(res: List[Tuple[int]], arr: np.ndarray, index: int, num: int, reduced: int) -> None:
    """Utility function used by `find_combinations()`. Modifies `res` in place."""
    if reduced < 0:
        return

    if reduced == 0:
        res.append(tuple(arr[:index]))  # type: ignore
        return

    prev = 1 if (index == 0) else arr[index - 1]

    for j in range(prev, num + 1):
        arr[index] = j
        _find_combinations_recursive(res, arr, index + 1, num, reduced - j)


def _find_combinations(target: int) -> List[Tuple[int]]:
    """Return all combinations of positive numbers that add to the `target`.

    The combinations will be sorted ascending by length (excluding the single length
    1 solution), and the values sorted ascending.

    Examples
    --------
    >>> _find_combinations(5)
    [[2, 3], [1, 4], [1, 2, 2], [1, 1, 3], [1, 1, 1, 2], [1, 1, 1, 1, 1]]
    """
    results: List[Tuple[int]] = []
    arr = np.zeros(target, dtype=int)

    _find_combinations_recursive(results, arr, 0, target, target)

    # sort results ascending by count, and remove first (count = 1)
    return results[::-1][1:]


class BlockSplitter:
    """Class to generate ways to split a block.

    Should be instantiated once and reset with the `reset()` method.
    """

    def __init__(self, max_size: int):
        """Initialise a `BlockSplitter` instance.

        Parameters
        ----------
        max_size
            The maximum size of a block.
        """
        self._splits_by_size = [_find_combinations(i) for i in range(1, max_size + 1)]

        self._diffs_by_size = []
        for splits in self._splits_by_size:
            diffs = []
            for split in splits:
                diff = np.zeros(max_size, dtype=np.int)
                for s in split:
                    diff[s - 1] += 1
                diffs.append(diff)
            self._diffs_by_size.append(diffs)

        self._split_indices = np.zeros(len(self._splits_by_size), dtype=int)

    def get_split(
        self, block_size: int, last_one_invalid: bool
    ) -> Union[Tuple[Tuple[int, ...], np.ndarray], Tuple[None, None]]:
        """Return a tuple of sub-block sizes and an representing their sizes.

        The array of sizes can be used to easily see how the remaining syndicate sizes
        would be modified.

        Parameters
        ----------
        block_size
            Size of the block to split.

        last_one_invalid
            `True` if the last split returned was not valid. This prompts the class
            to increment the appropriate index so that it is not returned again.
        """
        i = block_size - 1
        if last_one_invalid:
            self._split_indices[i] += 1
        j = self._split_indices[block_size - 1]

        try:
            return self._splits_by_size[i][j], self._diffs_by_size[i][j]
        except IndexError:
            return None, None

    def reset(self) -> None:
        """Reset the indices. Call this whenever a new `BlockConfig` is generated."""
        self._split_indices.fill(0)
