"""File containing the `find_valid_configs()` function."""

import time
from typing import List, Tuple, TYPE_CHECKING, Union

import numpy as np
from tqdm import tqdm

from src.ballot.shuffle.BlockConfig import BlockConfig, BlockConfigGenerator
from src.ballot.shuffle.BlockSplitter import BlockSplitter

if TYPE_CHECKING:
    from src.models import Block, Syndicate


def _get_sizes_and_count(items: Union[List["Block"], List["Syndicate"]], max_size: int) -> Tuple[np.ndarray, int]:
    """Return an array representing the distribution of item `size`s."""
    sizes = np.zeros(max_size, dtype=np.int)
    count = 0

    for item in items:
        if item.size > 0:
            sizes[item.size - 1] += 1
            count += item.size

    return sizes, count


def find_valid_configs(
    syndicates: List["Syndicate"], blocks: List["Block"], seeds: List[int], verbose: bool = True
) -> Tuple[List[BlockConfig], List[int]]:
    """Return a list of valid configs and the seeds used to generate them.

    This is achieved by looping through the given seeds (should just be a range
    for the first round), and generating a new `BlockConfig` instance for each.
    """
    start = time.time()

    max_block_size = max(block.size for block in blocks)
    max_size = max(max_block_size, *[syndicate.size for syndicate in syndicates])

    # need to use max of both to make size arrays the same length
    syndicate_sizes, num_students = _get_sizes_and_count(syndicates, max_size)
    block_sizes, num_rooms = _get_sizes_and_count(blocks, max_size)

    block_splitter = BlockSplitter(max_block_size)
    gaps_allowed = num_rooms - num_students

    if verbose:
        print(f"\nAttempting to fit {num_students} students into {num_rooms} rooms:")
        print(f"\tSyndicate sizes: {list(syndicate_sizes)}")
        print(f"\tBlock sizes:     {list(block_sizes)}")

    valid_configs = []
    valid_seeds = []

    for i, seed in enumerate(tqdm(seeds)):
        block_config_gen = BlockConfigGenerator(seed, blocks, syndicate_sizes, block_splitter, gaps_allowed)

        if block_config_gen.is_valid():
            valid_configs.append(block_config_gen.build())
            valid_seeds.append(seed)

    if verbose:
        print(f"\tFinished in {time.time() - start:.3f} s.")
        perc = len(valid_configs) / len(seeds) * 100
        print(f"\t{len(valid_configs)} / {len(seeds)} ({perc:.1f}%) were valid.")

    return valid_configs, valid_seeds
