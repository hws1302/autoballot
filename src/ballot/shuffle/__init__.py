"""Module containing functions to generate random block configurations.

It is very unlikely that the distribution of syndicate sizes will perfectly match the
distribution of block sizes, but this is required for the optimisation algorithm to
run. To get around this, the distribution of block sizes is modified by combining
and splitting blocks (where necessary and possible).

Combining a block is equivalent to splitting a syndicate over multiple blocks, but
to avoid separating the students IRL, only blocks in the same building will be
combined. Likewise, splitting a block is equivalent to combining syndicates, thus this
will be attempted last (since we want to avoid multiple syndicates sharing a block
where possible).
"""
