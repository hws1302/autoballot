"""Directory containing the source code for the project.

Each module represents a different stage in the running of the program:

1) Data processing - room and student data is read from the csv files by the functions
in the `readers` module. This data is stored as a collection of class instances, as
defined in the `models` module. Note that this module also contains some utility
functions used to calculate things like preference vectors.

2) Generate compatible block layouts for the given syndicate sizes - this is done by
the `ballot.shuffle` module.

3) Optimally assign students to rooms, and evaluate the different solutions - this
is done in the `ballot.evaluate` module. Student and syndicate matches are performed
and analysed in the `ballot.match` module.

4) Perform some additional validation - this is done in the `ballot.validate` module.
Here we check things that cannot be easily be enforced while the program runs, but
are unlikely to be too restrictive (e.g. certain people not being able to live
together).

The functions used to run each stage are stored in the `run` module, and those used
to summarise the results in the `results` module.
"""
