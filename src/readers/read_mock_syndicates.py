"""File containing the `read_mock_syndicates()` function."""

import os
from typing import Dict, List, Optional, Tuple, TYPE_CHECKING

import numpy as np
import pandas as pd

from src.ballot.evaluate.utils.anonymise import get_animals
from src.models import OTBRequirements, Student, Syndicate
from src.params import Constants, Preferences

if TYPE_CHECKING:
    from src.models import Room


def read_mock_syndicates(
    pre_allocated: Dict[str, "Room"], dir_path: str = "data/parsed_inputs"
) -> Tuple[List[Syndicate], List[Student]]:
    """Return a mock list of syndicates, students and pre-allocated students.

    This function is interchangeable with `read_syndicates()` and can be used for
    testing and profiling.

    Parameters
    ----------
    pre_allocated
        Dictionary of crsids to pre-allocated rooms.

    dir_path
        Path to the directory containing rooms.csv. The number of students returned
        will equal the number of rooms (representing the most difficult case). To
        remove students, delete a syndicate.
    """
    num_students = len(pd.read_csv(os.path.join(*os.path.split(dir_path), "rooms.csv")))
    animals = get_animals()

    np.random.seed(1)

    def p(_prob: float) -> bool:
        return _prob > np.random.rand()

    # syndicate size probabilities
    prob_syn_size = np.array([10, 7, 11, 6, 7, 6, 7, 5])
    prob_syn_size = prob_syn_size / sum(prob_syn_size)

    # requirement probabilities
    prob_otb = 0.1  # probability a student has requirements
    prob_reqs = dict(double=0.1, ensuite=0.6, inward=0.1, upper=0.1, long_lease=0.2, min_size=0)
    prob_long_lease = 0.1

    syndicates: Dict[str, Syndicate] = {}
    pre_syndicates: Dict[str, Syndicate] = {}
    student_list: List[Student] = []

    # preference probabilities
    pref_vals = [
        Preferences.not_at_all.value,
        Preferences.not_really.value,
        Preferences.indifferent.value,
        Preferences.a_little.value,
        Preferences.a_lot.value,
        Preferences.definitely.value,
    ]
    pref_probs = np.array([7, 7, 5, 3, 2, 1])
    pref_probs = pref_probs / sum(pref_probs)

    pref_alt_vals = [
        Preferences.negative.value,
        Preferences.probably_not.value,
        Preferences.indifferent.value,
        Preferences.probably.value,
        Preferences.positive.value,
    ]
    pref_alt_probs = np.array([7, 3, 3, 3, 7])
    pref_alt_probs = pref_alt_probs / sum(pref_alt_probs)

    # counters
    num_otb = 0
    num_long_lease = 0
    syn_sizes = np.zeros(len(prob_syn_size), dtype=int)

    # run
    syndicate_id = 0
    syn_left = 0
    wants_house = 0.0  # overridden
    wants_long_lease = 0.0  # overridden

    for i in range(num_students):
        # syndicate
        if syn_left == 0:
            syn_size = np.random.choice(range(1, len(prob_syn_size) + 1), p=prob_syn_size)
            syn_sizes[syn_size - 1] += 1
            syndicate_id += 1
            syn_left = syn_size - 1

            wants_house = np.random.choice(pref_alt_vals, p=pref_alt_probs)
            wants_long_lease = np.random.choice(pref_alt_vals, p=pref_alt_probs)
        else:
            syn_left -= 1

        # requirements
        otb_req: Optional[OTBRequirements] = None
        if p(prob_otb):
            num_otb += 1
            failed = True
            while failed:
                otb_req = OTBRequirements(
                    double=p(prob_reqs["double"]),
                    ensuite=p(prob_reqs["ensuite"]),
                    inward=p(prob_reqs["inward"]),
                    upper=p(prob_reqs["upper"]),
                    long_lease=p(prob_reqs["long_lease"]),
                    min_size=15 if p(prob_reqs["min_size"]) else 0,
                )
                failed = not any([otb_req.double, otb_req.ensuite, otb_req.inward, otb_req.upper, otb_req.min_size])
        elif p(prob_long_lease):
            num_long_lease += 1
            otb_req = OTBRequirements(
                double=False, ensuite=False, inward=False, upper=False, long_lease=True, min_size=0
            )

        # preferences
        est_price = 107.0

        wants_double = np.random.choice(pref_vals, p=pref_probs)
        wants_ensuite = np.random.choice(pref_vals, p=pref_probs)
        wants_upper = np.random.choice(pref_vals, p=pref_probs)
        wants_faces_court = np.random.choice(pref_vals, p=pref_probs)
        wants_not_faces_lr = np.random.choice(pref_vals, p=pref_probs)
        wants_new_kitchen = np.random.choice(pref_vals, p=pref_probs)
        wants_new_bathroom = np.random.choice(pref_vals, p=pref_probs)
        wants_new_room = np.random.choice(pref_vals, p=pref_probs)

        est_price += (
            11 * wants_double
            + 14 * wants_ensuite
            + 3 * wants_upper
            + 5 * wants_faces_court
            + 7 * wants_not_faces_lr
            + 6 * wants_new_kitchen
            + 6 * wants_new_bathroom
            + 10 * wants_new_room
        )

        min_size = np.random.randint(9, 15)
        max_size = np.random.randint(min_size, 40)
        y = (min_size + max_size) / (2 * 31)
        est_price += (25 * (2 * y - y ** 2)) * 1.20314
        est_price = min(211.0, max(130.0, est_price))

        # student
        student = Student(
            index=i,
            crsid=animals[i],
            rank=Constants.num_in_ballot.value,  # TODO include rank
            wants_double=wants_double,
            wants_ensuite=wants_ensuite,
            wants_upper=wants_upper,
            wants_faces_court=wants_faces_court,
            wants_not_faces_lr=wants_not_faces_lr,
            wants_new_kitchen=wants_new_kitchen,
            wants_new_bathroom=wants_new_bathroom,
            wants_new_room=wants_new_room,
            wants_min_size=min_size,
            wants_max_size=max_size,
            wants_house=wants_house,
            wants_long_lease=wants_long_lease,
            est_price=est_price,
            price_perc_over=np.random.randint(8, 30),
            otb_req=otb_req,
        )

        student_list.append(student)

        # don't add to syndicate if pre-allocated
        was_pre_allocated = False
        try:
            room = pre_allocated[student.crsid]
            student.pre_allocated = room
            room.pre_allocated = student
            was_pre_allocated = True
        except KeyError:
            pass

        cur_syndicates = pre_syndicates if was_pre_allocated else syndicates

        try:
            cur_syndicates[str(syndicate_id)].add_student(student)
        except KeyError:
            syndicate = Syndicate(str(syndicate_id))
            syndicate.add_student(student)
            cur_syndicates[str(syndicate_id)] = syndicate

    # finalise syndicates
    for syndicate in syndicates.values():
        syndicate.finalise()

        # check no pre-allocated students in syndicate dictionary
        assert all(s.pre_allocated is None for s in syndicate.students), f"Not pre-allocating syndicate {syndicate.id}"

    for syndicate in pre_syndicates.values():
        syndicate.finalise()

        # check pre-allocated students are with their syndicate
        student = syndicate.students[0]
        assert all(
            s.pre_allocated.building == student.pre_allocated.building for s in syndicate.students[1:]  # type: ignore
        ), f"Pre-allocating {student.crsid}"

    # print details
    print(f"OTB: {num_otb}, Long lease (only): {num_long_lease}")
    print(f"Syndicate sizes: {syn_sizes}")

    return list(syndicates.values()), student_list
