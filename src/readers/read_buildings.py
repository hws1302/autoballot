"""File containing the `read_buildings()` function."""

import os
from collections import defaultdict
from typing import DefaultDict, Dict, List, Tuple

import numpy as np
import pandas as pd

from src.models import Block, Building, Room
from src.models.utils.placeholders import placeholder_student


def read_buildings(dir_path: str = "data/parsed_inputs") -> Tuple[List[Building], List[Room], Dict[str, Room]]:
    """
    Return a list of buildings, rooms and pre-allocated crsids.

    Parameters
    ----------
    dir_path
        Path to the directory containing rooms.csv.
    """
    df = pd.read_csv(os.path.join(*os.path.split(dir_path), "rooms.csv")).replace({np.nan: None})

    # map renovation values to decimal (0 = oldest, 1 = newest)
    max_bathroom_age = 2020 - min(df["bathroom_ren"])
    df["new_bathroom"] = df["bathroom_ren"].apply(lambda x: 1 - (2020 - x) / max_bathroom_age)
    max_kitchen_age = 2020 - min(df["kitchen_ren"])
    df["new_kitchen"] = df["kitchen_ren"].apply(lambda x: 1 - (2020 - x) / max_kitchen_age)
    max_room_age = 2020 - min(df["room_ren"])
    df["new_room"] = df["room_ren"].apply(lambda x: 1 - (2020 - x) / max_room_age)

    # initialise blocks and buildings dictionaries
    blocks: DefaultDict[str, Dict[str, Block]] = defaultdict(lambda: {})
    buildings: Dict[str, Building] = {}

    room_list: List[Room] = []
    pre_allocated: Dict[str, Room] = {}

    for index, row in df.iterrows():
        # checks
        assert row["contract_length"] in {30, 38}

        room = Room(
            index=index,
            id=row["room_id"],  # 44LR06
            floor=row["floor"],  # indexed from 1
            is_double=bool(row["is_double"]),
            is_ensuite=bool(row["is_ensuite"]),
            faces_court=bool(row["faces_court"]),
            not_faces_lr=not bool(row["faces_lr"]),
            size=row["size"],
            band=row["band"],  # "0" = "1*"
            price=row["price"],
            is_house=row["is_house"],
            long_lease=row["contract_length"] == 38,
            new_bathroom=row["new_bathroom"],
            new_kitchen=row["new_kitchen"],
            new_room=row["new_room"],
        )

        if row["pre_allocated"] is not None:
            pre_allocated[row["pre_allocated"]] = room
            room.pre_allocated = placeholder_student(row["room_id"])  # needed to get full_size prop correct

        room_list.append(room)

        block_id = row["block_id"]
        building_id = row["building_id"]

        try:
            blocks[building_id][block_id].add_room(room)
        except KeyError:
            block = Block(block_id)
            block.add_room(room)
            blocks[building_id][block_id] = block

            try:
                buildings[building_id].add_block(block)
            except KeyError:
                building = Building(building_id)
                building.add_block(block)
                buildings[building_id] = building

    # finalise buildings and blocks
    for building in buildings.values():
        building.finalise()

    return list(buildings.values()), room_list, pre_allocated
