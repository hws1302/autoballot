"""File containing the `read_syndicates()` function."""

import os
from typing import Dict, List, Optional, Tuple, TYPE_CHECKING

import numpy as np
import pandas as pd

from src.models import OTBRequirements, Student, Syndicate

if TYPE_CHECKING:
    from src.models import Room


def read_syndicates(
    pre_allocated: Dict[str, "Room"], dir_path: str = "data/parsed_inputs"
) -> Tuple[List[Syndicate], List[Student]]:
    """Return a list of syndicates, students and pre-allocated students.

    Parameters
    ----------
    pre_allocated
        Dictionary of crsids to pre-allocated rooms.

    dir_path
        Path to the directory containing students.csv and otb.csv.
    """
    df = pd.read_csv(os.path.join(*os.path.split(dir_path), "students.csv")).replace({np.nan: None})
    otb_df = pd.read_csv(os.path.join(*os.path.split(dir_path), "otb.csv")).set_index("crsid").replace({np.nan: None})

    syndicates: Dict[str, Syndicate] = {}
    pre_syndicates: Dict[str, Syndicate] = {}
    student_list: List[Student] = []

    for index, row in df.iterrows():
        try:
            try:
                otb_row = otb_df.loc[row["crsid"]]
            except KeyError:
                raise NameError

            # quick check that the format is correct
            assert otb_row["need_ensuite"] in {"yes", "no", None}

            otb_req: Optional[OTBRequirements] = OTBRequirements(
                double=otb_row["need_double"] == "yes",
                ensuite=otb_row["need_ensuite"] == "yes",
                inward=otb_row["need_inward"] == "yes",
                upper=otb_row["need_upper"] == "yes",
                long_lease=otb_row["need_long_lease"] == "yes",
                min_size=otb_row["min_size"],
            )
            if otb_row["in_program"] != "yes":
                assert row["crsid"] in pre_allocated
        except NameError:
            otb_req = None

        student = Student(
            index=index,
            crsid=row["crsid"],
            rank=row["rank"],
            wants_double=row["wants_double"],
            wants_ensuite=row["wants_ensuite"],
            wants_upper=row["wants_upper"],
            wants_faces_court=row["wants_faces_court"],
            wants_not_faces_lr=row["wants_not_faces_lr"],
            wants_min_size=row["wants_min_size"],
            wants_max_size=row["wants_max_size"],
            wants_new_kitchen=row["wants_new_kitchen"],
            wants_new_bathroom=row["wants_new_bathroom"],
            wants_new_room=row["wants_new_room"],
            wants_house=row["wants_house"],
            wants_long_lease=row["wants_long_lease"],
            est_price=row["est_price"],
            price_perc_over=row["price_perc_over"],
            otb_req=otb_req,
        )

        student_list.append(student)
        syndicate_id = str(row["syndicate_id"])

        # don't add to syndicate if pre-allocated
        was_pre_allocated = False
        try:
            room = pre_allocated[student.crsid]
            student.pre_allocated = room
            room.pre_allocated = student
            was_pre_allocated = True
        except KeyError:
            pass

        cur_syndicates = pre_syndicates if was_pre_allocated else syndicates

        try:
            cur_syndicates[syndicate_id].add_student(student)
        except KeyError:
            syndicate = Syndicate(syndicate_id)
            syndicate.add_student(student)
            cur_syndicates[syndicate_id] = syndicate

    # finalise syndicates
    for syndicate in syndicates.values():
        syndicate.finalise()

        # check no pre-allocated students in syndicate dictionary
        assert all(s.pre_allocated is None for s in syndicate.students), f"Not pre-allocating syndicate {syndicate.id}"

    for syndicate in pre_syndicates.values():
        syndicate.finalise()

        # check pre-allocated students are with their syndicate
        student = syndicate.students[0]
        assert all(
            s.pre_allocated.building == student.pre_allocated.building for s in syndicate.students[1:]  # type: ignore
        ), f"Pre-allocating {student.crsid}"

    return list(syndicates.values()), student_list
