"""Script to analyse the previous room allocations."""

import numpy as np

from src.ballot.evaluate.analyse_solution import analyse_solution
from src.ballot.match.StudentMatches import StudentMatches
from src.models.utils.to_dataframes import buildings_to_dataframes, syndicates_to_dataframes
from src.readers.read_buildings import read_buildings
from src.readers.read_syndicates import read_syndicates
from src.run.analyse_in_dtale import analyse_in_dtale


#%%
def analyse_old() -> None:
    buildings, room_list, pre_allocated = read_buildings()
    syndicates, student_list = read_syndicates(pre_allocated)

    dfs = {}
    dfs["rooms"], dfs["blocks"], dfs["buildings"] = buildings_to_dataframes(buildings)
    dfs["students"], dfs["syndicates"] = syndicates_to_dataframes(syndicates)

    for room in room_list:
        if isinstance(room.pre_allocated, str):
            room.pre_allocated = None

    student_matches = StudentMatches()
    df = analyse_solution(student_list, room_list, np.zeros(600), student_matches, anon=False)

    df = df[~df.index.duplicated(keep="first")]
    df["cost_rank"] = df.apply(lambda r: r["cost"] / (1 + 9 * (230 - r["rank"]) / 230), axis=1)
    df["lim_cost"] = df["cost"].apply(lambda x: min(x, 250))

    analyse_in_dtale(df)

    print("Num got double =", len(df[(df["wants_double"] > 0.3) & (df["is_double"])]))
    print("Num got ensuite =", len(df[(df["wants_ensuite"] > 0.3) & (df["is_ensuite"])]))
    print("Num got upper =", len(df[(df["wants_upper"] > 0.3) & (df["is_upper"])]))
    print()
    print("Num miss faces court =", len(df[(df["wants_faces_court"] > 0.3) & (df["faces_court"])]))
    print("Num miss not faces lr =", len(df[(df["wants_not_faces_lr"] > 0.3) & (df["not_faces_lr"])]))
    print()
    print("Num got new kitchen =", len(df[(df["wants_double"] > 0.3) & (df["new_kitchen"] > 0.75)]))
    print("Num got new bathroom =", len(df[(df["wants_new_bathroom"] > 0.3) & (df["new_bathroom"] > 0.75)]))
    print("Num got new room =", len(df[(df["wants_new_room"] > 0.3) & (df["new_room"] > 0.75)]))
