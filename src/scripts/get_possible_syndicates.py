"""Script to generate a nicely formatted csv of syndicates who are allowed to stay."""

import pandas as pd


def get_possible_syndicates() -> None:
    df = pd.read_csv("data/old_rooms.csv")

    for index, row in df.iterrows():
        if row["crsid"] is not None:
            others = df[df["block_id"] == row["block_id"]]["crsid"]

            if not any(others.isnull()):
                df.at[index, "syndicate"] = "; ".join(sorted(others.values))

        others = df[df["block_id"] == row["block_id"]]["room_id"]

        r = (
            "; ".join(sorted(others.values))
            .replace(df.iloc[index]["house"], "")
            .replace("LR", "-")
            .replace("RS", "")
            .replace("SB", "")
            .replace(" -", " ")
            .replace(" 0", " ")
            .replace("-0", "-")
        )

        df.at[index, "rooms"] = r[1:] if r[0] in {"-", "0"} else r

    df.drop(["block_num", "crsid", "room_id"], axis=1).to_csv("data/poss_syndicates.csv")
    df.groupby("block_id")["room_id"].transform(lambda x: ", ".join(x)).drop_duplicates().to_csv("data/nice_blocks.csv")
