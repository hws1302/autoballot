"""Script to identify syndicates with incompatible price preferences."""

import numpy as np

from src.readers.read_buildings import read_buildings
from src.readers.read_syndicates import read_syndicates


#%%
def find_syndicates_low_price_otb() -> None:
    _, _, pre_allocated = read_buildings()
    syndicates, _ = read_syndicates(pre_allocated)

    for syndicate in sorted(syndicates, key=lambda s: s.id):
        otb_ensuite = False
        min_price = 1000.0

        for student in syndicate.students:
            min_price = min(min_price, student.est_price)

            if student.otb_req is not None:
                if student.otb_req.ensuite:
                    otb_ensuite = True

        if otb_ensuite and min_price < 155:
            print(f"£{min_price:.2f} - {syndicate}")


#%%
def find_syndicates_big_price_var() -> None:
    _, _, pre_allocated = read_buildings()
    syndicates, _ = read_syndicates(pre_allocated)

    for syndicate in sorted(syndicates, key=lambda s: s.id):
        prices = np.array([s.est_price for s in syndicate.students])

        sigma = np.var(prices) ** 0.5
        if sigma > 15:
            print(f"{sigma:.1f} - {syndicate}")
