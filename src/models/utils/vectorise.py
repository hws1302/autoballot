"""File containing functions to convert facilities and preferences to vectors."""

from typing import TYPE_CHECKING

import numpy as np

from src.params import Vectors

if TYPE_CHECKING:
    from src.models import Room, Student


_WEIGHT = np.array(
    [
        Vectors.double.value,
        Vectors.ensuite.value,
        Vectors.upper.value,
        Vectors.faces_lr.value,
        Vectors.faces_court.value,
        Vectors.new_kitchen.value,
        Vectors.new_bathroom.value,
        Vectors.new_room.value,
        Vectors.house.value,
        Vectors.long_lease.value,
    ]
)


def student_to_vec(student: "Student") -> np.ndarray:
    """Return a vector representation of the `student`'s preferences.

    It is assumed that the values assigned to the student's preferences are floats
    between 0 and 1, and as such no processing is done in this function.

    The preferences vector only accounts for boolean parameters (e.g. ensuite, double)
    NOT numerical ones (e.g. size, price).
    """
    return np.array(
        [
            student.wants_double,
            student.wants_ensuite,
            student.wants_upper,
            student.wants_not_faces_lr,
            student.wants_faces_court,
            student.wants_new_kitchen,
            student.wants_new_bathroom,
            student.wants_new_room,
            student.wants_house,
            student.wants_long_lease,
        ]
    )


def room_to_vec(room: "Room") -> np.ndarray:
    """Return a vector representation of the `rooms`'s facilities / features.

    The preferences vector only accounts for boolean parameters (e.g. ensuite, double)
    NOT numerical ones (e.g. size, price)

    Notes
    -----
    Increasing the `weight` of a facility means that a student will be considered
    'happier' with the facility or 'sadder' without it. For example, setting the
    value corresponding to the presence of a double bed to a higher number than that
    for an ensuite means that students will be assumed to 'care more' about double beds.
    """
    a = np.array(
        [
            room.is_double,
            room.is_ensuite,
            room.is_upper,
            room.not_faces_lr,
            room.faces_court,
            room.new_kitchen * 2 - 1,
            room.new_bathroom * 2 - 1,
            room.new_room * 2 - 1,
            int(room.is_house),
            int(room.long_lease),
        ]
    )
    a[a == 0] = -1
    return np.multiply(a, _WEIGHT)
