"""File containing functions to generate summary dataframes for lists of models."""

from typing import Any, Dict, List, Tuple, TYPE_CHECKING

import pandas as pd

if TYPE_CHECKING:
    from src.models import Building, Syndicate


def _models_to_dicts(models: List[Any], attrs: List[str]) -> List[Dict[str, Any]]:
    """Return a list of dictionaries that can be used with `pd.DataFrame.from_records`.

    Parameters
    ----------
    models
        List of models whose parameters are to be extracted.

    attrs
        List of attributes to extract. Chained attributes can be specified using ":",
        e.g. `block.building.id` can be accessed with "building:id". List attributes
        can be summarised using "...", e.g. "students...crsid" generates a comma
        separated string of student crsids.
    """
    dicts = []
    for model in models:
        entry = {}
        for key in attrs:
            value = model
            if "..." in key:
                key, sub_attr = key.split("...")
                value = ", ".join(sorted(getattr(sub_val, sub_attr) for sub_val in getattr(value, key)))
            else:
                for sub_key in key.split(":"):
                    value = getattr(value, sub_key)
            entry[key.replace("__", "")] = value
        dicts.append(entry)
    return dicts


def buildings_to_dataframes(buildings: List["Building"]) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    """Return dataframes summarising the rooms, blocks and buildings."""
    buildings_df = pd.DataFrame.from_records(
        [_models_to_dicts(buildings, ["id", "num_blocks", "num_rooms", "blocks...id"])]
    )

    blocks = [bl for bd in buildings for bl in bd.blocks]
    blocks_df = pd.DataFrame.from_records(
        _models_to_dicts(blocks, ["id", "building:id", "is_combined", "is_split", "size", "rooms...id"])
    )

    rooms = [r for bd in buildings for bl in bd.blocks for r in bl.rooms]
    room_keys = [k for k in vars(rooms[0]).keys() if not k.startswith("_")] + ["block:id", "building:id"]
    rooms_df = pd.DataFrame.from_records(_models_to_dicts(rooms, room_keys))

    return rooms_df, blocks_df, buildings_df


def syndicates_to_dataframes(syndicates: List["Syndicate"]) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """Return dataframes summarising the students and syndicates."""
    syndicates_df = pd.DataFrame.from_records(_models_to_dicts(syndicates, ["id", "size", "students...crsid"]))

    students = [st for sy in syndicates for st in sy.students]
    student_keys = [k for k in vars(students[0]).keys() if not k.startswith("_")] + ["syndicate:id"]
    students_df = pd.DataFrame.from_records(_models_to_dicts(students, student_keys))

    return students_df, syndicates_df
