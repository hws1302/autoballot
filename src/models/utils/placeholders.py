"""File containing functions to generate placeholder model instances."""

from src.models import Student
from src.params import Preferences


def placeholder_student(source: str = "n/a") -> Student:
    """Return a placeholder `Student` instance."""
    v = Preferences.indifferent.value
    return Student(
        index=hash(source),
        crsid=source,
        rank=-1,
        wants_double=v,
        wants_ensuite=v,
        wants_upper=v,
        wants_faces_court=v,
        wants_not_faces_lr=v,
        wants_min_size=15,
        wants_max_size=16,
        wants_new_kitchen=v,
        wants_new_bathroom=v,
        wants_new_room=v,
        wants_house=v,
        wants_long_lease=v,
        est_price=150,
        price_perc_over=10,
        otb_req=None,
    )
