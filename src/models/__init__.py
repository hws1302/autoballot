"""Module containing classes to define objects relevant to the program.

This includes: students; syndicates of students; rooms; isolated blocks of rooms; and
buildings containing one or more blocks.
"""

from src.models._Block import Block, CBlock, SBlock, SCBlock
from src.models._Building import Building
from src.models._Group import Group
from src.models._Room import Room
from src.models._Student import OTBRequirements, Student
from src.models._Syndicate import Syndicate

# show private modules in pdoc3
__pdoc__ = dict(_Block=True, _Building=True, _Group=True, _Room=True, _Student=True, _Syndicate=True)
