"""File containing the `Building` class."""

from __future__ import annotations

import warnings
from typing import Any, List, Optional, TYPE_CHECKING

from src.models._Group import Group

if TYPE_CHECKING:
    from src.models import Block


class Building(Group):
    """Class representing a building in college.

    Joined houses are considered to be one building (e.g. '44/46').
    """

    def __init__(self, _id: str):
        """Initialise a `Building` instance."""
        super().__init__(_id)
        self._num_rooms: Optional[int] = None

    def __eq__(self, other: Any) -> bool:
        return isinstance(other, Building) and self.id == other.id

    def finalise(self) -> None:
        """Mark the building as 'finalised', allowing its properties to be read.

        Note that all its blocks will be finalised first.
        """
        self._num_rooms = 0
        for block in self.blocks:
            block.finalise()
            self._num_rooms += block.size

        super().finalise()

    @property
    def blocks(self) -> List["Block"]:
        """The blocks in the building."""
        return self._items

    @property
    def size(self) -> int:
        """The number of blocks in the building. `num_blocks` should be used instead."""
        warnings.warn("Property 'size' for a building is ambiguous. Use 'num_blocks' instead.")
        return self.num_blocks

    @property
    def num_blocks(self) -> int:
        """The number of blocks in the building."""
        return super().size

    @property
    def num_rooms(self) -> int:
        """The number of rooms in the building."""
        assert self._num_rooms is not None
        return self._num_rooms

    def add_block(self, block: "Block") -> None:
        """Add a `block` to the building. The `block`'s building will be set."""
        self._add_item(block)
        block.set_building(self)
