"""File containing the `Room` class."""

from __future__ import annotations

from dataclasses import dataclass, field
from typing import Any, Optional, TYPE_CHECKING

import numpy as np

from src.models.utils.vectorise import room_to_vec

if TYPE_CHECKING:
    from src.models import Block, Building, Student


@dataclass(init=True, repr=True, eq=False, order=False, frozen=False)
class Room:
    """Class representing a room in college."""

    # attributes to be initialised
    index: int

    id: str
    floor: int

    is_double: bool
    is_ensuite: bool
    faces_court: bool
    not_faces_lr: bool
    is_house: bool
    size: float

    price: int
    band: int  # "0" = "1*"
    long_lease: bool

    # note we don't include 'bathroom share' as this is decided by syndicate size
    # 0 = oldest, 1 = newest
    new_bathroom: float
    new_kitchen: float
    new_room: float

    # attributes to be assigned
    _block: Optional["Block"] = field(default=None, init=False, repr=False)
    pre_allocated: Optional["Student"] = field(default=None, init=False)

    def __post_init__(self) -> None:
        self.is_upper = self.floor >= 2
        self._vec = room_to_vec(self)

        assert self.floor in {1, 2, 3, 4}
        assert self.band in set(range(13))
        assert 9 <= self.size <= 40
        for new in [self.new_bathroom, self.new_kitchen, self.new_room]:
            assert 0 <= new <= 1

    def __eq__(self, other: Any) -> bool:
        return isinstance(other, Room) and self.id == other.id

    def set_block(self, block: "Block") -> None:
        """Set the room's `block`.

        Note that this should be its original block, not a modified block.
        """
        assert self._block is None, f"Room in another block '{self.block.id}'"
        self._block = block

    @property
    def block(self) -> "Block":
        """The original block of rooms in which this room is found."""
        assert self._block is not None
        return self._block

    @property
    def building(self) -> "Building":
        """The associated building."""
        return self.block.building

    @property
    def vec(self) -> np.ndarray:
        """A vector representation of the room's facilities."""
        return self._vec
