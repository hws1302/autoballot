"""File containing the `Syndicate` class."""

from __future__ import annotations

from typing import List, TYPE_CHECKING

from src.models._Group import Group

if TYPE_CHECKING:
    from src.models import Student


class Syndicate(Group):
    """Class representing a syndicate (i.e. a group of students)."""

    @property
    def students(self) -> List["Student"]:
        """The students in the syndicate."""
        return self._items

    def add_student(self, student: "Student") -> None:
        """Add a `student` to the syndicate. The `student`'s syndicate will be set."""
        self._add_item(student)
        student.set_syndicate(self)
