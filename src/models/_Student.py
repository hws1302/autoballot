"""File containing the `Student` and `OTBRequirements` classes."""

from __future__ import annotations

from dataclasses import dataclass, field
from typing import Any, Optional, TYPE_CHECKING

import numpy as np

from src.models.utils.vectorise import student_to_vec
from src.params import Preferences

if TYPE_CHECKING:
    from src.models import Room, Syndicate


@dataclass(init=True, repr=False, eq=False, order=False, frozen=True)
class OTBRequirements:
    """Class representing possible out the ballot student requirements.

    Notes
    -----
    While this could be made more general by accepting a variable number of keyword
    arguments and matching them to room features by name, it was felt that this more
    explicit method is less likely to introduce bugs.
    """

    double: bool
    ensuite: bool
    inward: bool
    upper: bool
    long_lease: bool
    min_size: float

    def __repr__(self) -> str:
        r = "OTB("

        if self.double:
            r += "double, "

        if self.ensuite:
            r += "ensuite, "

        if self.inward:
            r += "inward, "

        if self.upper:
            r += "upper, "

        if self.upper:
            r += "long lease, "

        if self.min_size != 0:
            r += f">{self.min_size}, "

        return r[:-2] + ")"

    def is_missing_requirements(self, room: "Room") -> bool:
        """Return `True` if the given `room` is missing one or more requirements."""
        return (
            (self.double and not room.is_double)
            or (self.ensuite and not room.is_ensuite)
            or (self.upper and not room.is_upper)
            or (self.inward and not room.not_faces_lr)
            or (self.long_lease and not room.long_lease)
            or (self.min_size > room.size)
        )


@dataclass(init=True, repr=True, eq=False, order=False, frozen=False)
class Student:
    """Class representing a student."""

    # attributes to be initialised
    index: int
    crsid: str
    rank: float  # ballot rank or another weighting factor

    # boolean preferences, as float between -1 and 1
    # -1 means strongly don't want, 1 means strongly want, 0 means indifferent
    wants_double: float
    wants_ensuite: float
    wants_upper: float
    wants_faces_court: float
    wants_not_faces_lr: float  # probably negative
    wants_new_kitchen: float
    wants_new_bathroom: float
    wants_new_room: float
    wants_house: float
    wants_long_lease: float

    # numerical preferences
    wants_min_size: float  # meters squared, [9, wants_max_size]
    wants_max_size: float  # meters squared, [wants_min_size, 40]

    est_price: float  # estimated price per week, based on preferences
    price_perc_over: float  # percentage by which they are willing to overpay

    # out the ballot requirements
    otb_req: Optional[OTBRequirements]

    # attributes to be set
    _syndicate: Optional["Syndicate"] = field(default=None, init=False, repr=False)
    pre_allocated: Optional["Room"] = field(default=None, init=False)

    def __post_init__(self) -> None:
        self._vec = student_to_vec(self)

        for attr in [
            self.wants_double,
            self.wants_ensuite,
            self.wants_upper,
            self.wants_faces_court,
            self.wants_not_faces_lr,
            self.wants_new_kitchen,
            self.wants_new_bathroom,
            self.wants_new_room,
        ]:
            assert round(attr, 3) in {
                Preferences.not_at_all.value,
                Preferences.not_really.value,
                Preferences.indifferent.value,
                Preferences.a_little.value,
                Preferences.a_lot.value,
                Preferences.definitely.value,
            }, attr

        for attr in [self.wants_house, self.wants_long_lease]:
            assert round(attr, 3) in {
                Preferences.negative.value,
                Preferences.probably_not.value,
                Preferences.indifferent.value,
                Preferences.probably.value,
                Preferences.positive.value,
            }, attr

        assert 9 <= self.wants_min_size <= self.wants_max_size <= 40, f"{self.wants_min_size}, {self.wants_max_size}"
        assert 130 <= self.est_price <= 211, self.est_price
        assert 8 <= self.price_perc_over <= 100, self.price_perc_over

    def __eq__(self, other: Any) -> bool:
        return isinstance(other, Student) and other.crsid == other.crsid

    @property
    def vec(self) -> np.ndarray:
        """A vector representation of the student's preferences."""
        return self._vec

    @property
    def id(self) -> str:
        """Equal to crsid. Standardises the interface used by the `Matches` subclasses."""
        return self.crsid  # TODO use id instead of crsid in __init__()

    def set_syndicate(self, syndicate: "Syndicate") -> None:
        """Set the student's syndicate."""
        assert self._syndicate is None, f"Student is in another syndicate '{self.syndicate.id}'"
        self._syndicate = syndicate

    @property
    def syndicate(self) -> "Syndicate":
        """The student's syndicate."""
        assert self._syndicate is not None
        return self._syndicate
