"""File containing the abstract `Group` class."""

from abc import ABC
from typing import Any, Generic, List, Optional, TYPE_CHECKING, TypeVar

if TYPE_CHECKING:
    pass

TItem = TypeVar("TItem")


class Group(ABC, Generic[TItem]):
    """Abstract base class from which `Syndicate`, `Block` and `Building` are extended."""

    def __init__(self, _id: str):
        """Initialise a `Group` subclass."""
        self._id = _id
        self._items: List[TItem] = []
        self._size: Optional[int] = None

    def __eq__(self, other: Any) -> bool:
        return isinstance(other, self.__class__) and self.id == other.id

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(id={self.id},size={self.size}){self._items}"

    def finalise(self) -> None:
        """Mark the group as 'finalised', preventing more items from being added."""
        assert self._size is None
        self._size = len(self._items)

    @property
    def id(self) -> str:
        """The unique identifier of the group."""
        return self._id

    @property
    def size(self) -> int:
        """The number of items in the group."""
        assert self._size is not None
        return self._size

    def _add_item(self, item: TItem) -> None:
        assert self._size is None
        self._items.append(item)
