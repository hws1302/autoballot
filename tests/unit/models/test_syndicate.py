import pytest

from src.models import Student, Syndicate

default_student_kwargs = dict(
    rank=1,
    wants_double=0,
    wants_ensuite=0,
    wants_upper=0,
    wants_faces_court=0,
    wants_not_faces_lr=0,
    wants_new_kitchen=0,
    wants_new_bathroom=0,
    wants_new_room=0,
    wants_house=0,
    wants_long_lease=0,
    wants_min_size=9,
    wants_max_size=40,
    est_price=150,
    price_perc_over=20,
    otb_req=None,
)


def new_student(index, kwargs=None):
    if kwargs is None:
        kwargs = {}

    return Student(index=index, crsid=f"crs{index}", **dict(default_student_kwargs, **kwargs))


# =========== syndicate.add_student() =========== #


@pytest.mark.parametrize("size", [0, 1, 3, 8])
def test_syndicate_size_correct(size):
    syndicate = Syndicate("a")

    for i in range(size):
        syndicate.add_student(new_student(i + 1))

    syndicate.finalise()
    assert syndicate.size == size


def test_add_to_finalised_syndicate_raises():
    student = new_student(1)
    syndicate = Syndicate("a")
    syndicate.finalise()

    with pytest.raises(AssertionError):
        syndicate.add_student(student)


def test_add_to_syndicate_twice_raises():
    student = new_student(1)
    syndicate = Syndicate("a")

    syndicate.add_student(student)
    with pytest.raises(AssertionError):
        syndicate.add_student(student)


def test_add_to_two_syndicates_raises():
    student = new_student(1)
    syndicate_1 = Syndicate("a")
    syndicate_2 = Syndicate("b")

    syndicate_1.add_student(student)
    with pytest.raises(AssertionError):
        syndicate_2.add_student(student)


# =========== syndicate getters =========== #


def test_non_finalised_syndicate_getters_raise():
    syndicate = Syndicate("a")

    with pytest.raises(AssertionError):
        _ = syndicate.size
