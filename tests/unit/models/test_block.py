from typing import Any, Dict, Tuple

import pytest

from src.models import Block, Building, Room

default_room_kwargs = dict(
    floor=1,
    is_double=False,
    is_ensuite=False,
    faces_court=False,
    not_faces_lr=False,
    is_house=False,
    size=15,
    price=150,
    band=5,
    long_lease=False,
    new_bathroom=0.5,
    new_kitchen=0.5,
    new_room=0.5,
)


def new_room(index: int, kwargs: Dict[str, Any] = None):
    if kwargs is None:
        kwargs = {}

    return Room(index=index, id=str(index), **dict(default_room_kwargs, **kwargs))


def new_building(building_id: str):
    return Building(building_id)


def full_block(block_id: int, num_rooms: int, room_kwargs: Dict[str, Any] = None, finalise: bool = True):
    block = Block(str(block_id))

    for i in range(num_rooms):
        block.add_room(new_room(i + block_id, room_kwargs))

    if finalise:
        block.finalise()
    return block


# =========== block.add_room() =========== #


@pytest.mark.parametrize("size", [0, 1, 3, 8])
def test_block_size_correct(size):
    block = Block("a")

    for i in range(size):
        block.add_room(new_room(i + 1))

    block.finalise()
    assert block.size == size


def test_add_to_finalised_block_raises():
    room = new_room(1)
    block = Block("a")
    block.finalise()

    with pytest.raises(AssertionError):
        block.add_room(room)


def test_add_to_block_twice_raises():
    room = new_room(1)
    block = Block("a")

    block.add_room(room)
    with pytest.raises(AssertionError):
        block.add_room(room)


def test_add_to_two_blocks_raises():
    room = new_room(1)
    block_1 = Block("a")
    block_2 = Block("b")

    block_1.add_room(room)
    with pytest.raises(AssertionError):
        block_2.add_room(room)


# =========== block getters =========== #


def test_non_finalised_block_getters_raise():
    block = Block("a")

    with pytest.raises(AssertionError):
        _ = block.size


# =========== block.combine() =========== #


@pytest.mark.parametrize("size_1,size_2", [(1, 1), (3, 1), (5, 5), (1, 20)])
def test_combine_blocks_correct(size_1, size_2):
    block_1 = full_block(1, size_1)
    block_2 = full_block(100, size_2)

    building = Building("a")
    building.add_block(block_1)
    building.add_block(block_2)

    new_block = block_1.combine(block_2)
    assert new_block.building == building
    assert new_block.is_combined
    assert not new_block.is_split

    assert new_block.size == size_1 + size_2


def test_combine_blocks_in_different_buildings_raises():
    block_1 = full_block(1, 5, finalise=False)
    building_1 = Building("a")
    building_1.add_block(block_1)
    building_1.finalise()

    block_2 = full_block(10, 5, finalise=False)
    building_2 = Building("b")
    building_2.add_block(block_2)
    building_2.finalise()

    with pytest.raises(AssertionError):
        block_1.combine(block_2)


def test_combine_block_with_self_raises():
    block = full_block(1, 5)
    block_copy = full_block(1, 5)

    with pytest.raises(AssertionError):
        block.combine(block_copy)


# =========== block.split() =========== #


@pytest.mark.parametrize("splits", [(1, 1), (3, 1, 3), (5, 5, 1), (1, 20, 20)])
def test_split_blocks_correct(splits: Tuple[int, ...]):
    block = full_block(1, sum(splits))
    building = Building("a")
    building.add_block(block)

    room_lists = []
    lb = 0
    for s in splits:
        room_lists.append(block.rooms[lb : lb + s])
        lb += s

    for i, new_block in enumerate(block.split(room_lists)):
        assert new_block.building == building
        assert not new_block.is_combined
        assert new_block.is_split

        assert new_block.size == splits[i]


def test_split_block_zero_size_raises():
    block = full_block(1, 5)
    block.set_building(Building("a"))

    with pytest.raises(AssertionError):
        block.split([block.rooms, []])

    with pytest.raises(AssertionError):
        block.split([block.rooms[:2], [], block.rooms[2:]])


def test_split_block_too_few_raises():
    block = full_block(1, 5)
    block.set_building(Building("a"))

    with pytest.raises(AssertionError):
        block.split([block.rooms[:2], block.rooms[3:]])

    with pytest.raises(AssertionError):
        block.split([block.rooms[:1], block.rooms[2:4], block.rooms[4:]])


def test_split_block_too_many_raises():
    block = full_block(1, 5)
    block.set_building(Building("a"))

    with pytest.raises(AssertionError):
        block.split([block.rooms[:2], block.rooms[1:]])

    with pytest.raises(AssertionError):
        block.split([block.rooms[:1], block.rooms[1:4], block.rooms[3:]])


def test_split_block_overlap_raises():
    block = full_block(1, 5)
    block.set_building(Building("a"))

    with pytest.raises(AssertionError):
        block.split([block.rooms[:2], block.rooms[1:4]])

    with pytest.raises(AssertionError):
        block.split([block.rooms[:2], block.rooms[:3]])

    with pytest.raises(AssertionError):
        block.split([block.rooms[:1], block.rooms[:3], block.rooms[:1]])

    with pytest.raises(AssertionError):
        block.split([block.rooms[:1], block.rooms[1:4], block.rooms[2:3]])
