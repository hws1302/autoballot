from src.readers.read_buildings import read_buildings
from src.readers.read_syndicates import read_syndicates


def read_data():
    buildings, room_list, pre_allocated = read_buildings(dir_path="tests/mock_data")
    syndicates, student_list = read_syndicates(pre_allocated, dir_path="tests/mock_data")
    return buildings, room_list, syndicates, student_list
