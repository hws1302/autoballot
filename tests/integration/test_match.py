"""Integration tests for the `src.ballot.match` model.

Note that only the first 7 students and rooms (A-G) are included in this test.
"""

import copy
import itertools

import numpy as np
import pytest

from src.ballot.match.StudentMatches import StudentMatches
from src.ballot.match.SyndicateMatches import SyndicateMatches
from src.params import Constants, Preferences
from tests.integration.utils import read_data

_pref_feature_attrs = [
    ("wants_double", "is_double"),
    ("wants_ensuite", "is_ensuite"),
    ("wants_upper", "is_upper"),
    ("wants_faces_court", "faces_court"),
    ("wants_not_faces_lr", "not_faces_lr"),
    ("wants_new_kitchen", "new_kitchen"),
    ("wants_new_bathroom", "new_bathroom"),
    ("wants_new_room", "new_room"),
]

MAX_STUDENTS = 7


def _get_student_room_combinations():
    _, room_list, _, student_list = read_data()
    return list(itertools.product(student_list[:MAX_STUDENTS], room_list[:MAX_STUDENTS]))


def _get_student_room_pref_feature_combinations():
    _, room_list, _, student_list = read_data()
    return list(itertools.product(student_list[:MAX_STUDENTS], room_list[:MAX_STUDENTS], _pref_feature_attrs))


def _update_student(student_copy, room, pref, value):
    setattr(student_copy, pref, value)
    student_copy.crsid += f"{room.id}:{pref}={value}"  # needed to reusing old match
    student_copy.__post_init__()


def _update_room(room_copy, student, feature, value):
    if feature == "is_upper":
        setattr(room_copy, "floor", value + 1)
    else:
        setattr(room_copy, feature, value)  # ok for scalars as sets to 1
    room_copy.id += f"{student.crsid}:{feature}={value}"  # needed to reusing old match
    room_copy.__post_init__()


def _rand_lower_preference(student, pref):
    return np.random.randint(Preferences.not_at_all.value * 5, getattr(student, pref) * 5 - 1) / 5


def _rand_higher_pref(student, pref):
    return np.random.randint(getattr(student, pref) * 5 + 1, Preferences.definitely.value * 5) / 5


def test_correct_allocations():
    """Matches should be "along the diagonal", i.e. Aardvark -> A, Badger -> B etc."""
    student_matches = StudentMatches()
    syndicate_matches = SyndicateMatches(student_matches)

    buildings, room_list, syndicates, student_list = read_data()
    match = syndicate_matches.match(syndicates[0], buildings[0].blocks[0])

    for i, student_match in enumerate(match.matches):
        assert student_match.student == student_list[i]
        assert student_match.room == room_list[i]


@pytest.mark.parametrize("student,room", _get_student_room_combinations())
def test_price_penalties_arbitrarily_high(student, room):
    """Cost values should be very high where students overpay significantly."""
    student_matches = StudentMatches()
    if room.price >= student.est_price * (1 + student.price_perc_over / 100) + 15:
        assert student_matches.match(student, room).cost > 1000


@pytest.mark.parametrize("student,room,pref_feature", _get_student_room_pref_feature_combinations())
def test_changing_preference_cost_correct(student, room, pref_feature):
    pref, feature = pref_feature
    np.random.seed(hash(student.crsid + room.id + pref) % 2 ** 31)

    student_matches = StudentMatches()
    new_student = copy.copy(student)
    new_room = copy.copy(room)

    if getattr(student, pref) > Preferences.indifferent.value:
        # preference & feature
        if getattr(room, feature) > 0.5:
            # decreasing met preference -> increased cost
            _update_student(new_student, room, pref, _rand_lower_preference(student, pref))
            assert student_matches.match(new_student, room).cost > student_matches.match(student, room).cost

            # removing preferred feature -> increased cost
            _update_room(new_room, student, feature, False)
            assert student_matches.match(student, new_room).cost > student_matches.match(student, room).cost

        # preference & !feature
        else:
            # decreasing unmet preference -> decreased cost
            _update_student(new_student, room, pref, _rand_lower_preference(student, pref))
            assert student_matches.match(new_student, room).cost < student_matches.match(student, room).cost

            # adding preferred feature -> decreased cost
            _update_room(new_room, student, feature, True)
            assert student_matches.match(student, new_room).cost < student_matches.match(student, room).cost

    # includes indifferent
    else:
        # !preference & feature
        if getattr(room, feature) > 0.5:
            # increasing met preference -> decreased cost
            _update_student(new_student, room, pref, _rand_higher_pref(student, pref))
            assert student_matches.match(new_student, room).cost < student_matches.match(student, room).cost

            # removing disliked feature -> decreased cost. Indifferent -> no change
            _update_room(new_room, student, feature, False)
            if getattr(student, pref) == Preferences.indifferent.value:
                assert student_matches.match(student, new_room).cost == student_matches.match(student, room).cost
            else:
                assert student_matches.match(student, new_room).cost < student_matches.match(student, room).cost

        # !preference & !feature
        else:
            # increasing unmet preference -> increased cost
            _update_student(new_student, room, pref, _rand_higher_pref(student, pref))
            assert student_matches.match(new_student, room).cost > student_matches.match(student, room).cost

            # adding disliked feature -> increased cost. Indifferent -> no change
            _update_room(new_room, student, feature, True)
            if getattr(student, pref) == Preferences.indifferent.value:
                assert student_matches.match(student, new_room).cost == student_matches.match(student, room).cost
            else:
                assert student_matches.match(student, new_room).cost > student_matches.match(student, room).cost


@pytest.mark.parametrize("student,room", _get_student_room_combinations())
def test_changing_size_cost_correct(student, room):
    np.random.seed(hash(student.crsid + room.id) % 2 ** 31)

    student_matches = StudentMatches()
    feature = "size"

    # room too small
    if student.wants_min_size > room.size:
        pref = "wants_min_size"

        # decrease min size -> decreased cost
        new_student = copy.copy(student)
        _update_student(new_student, room, pref, np.random.randint(9, student.wants_min_size - 1))
        assert student_matches.match(new_student, room).cost < student_matches.match(student, room).cost

        # increase room size -> decreased cost
        new_room = copy.copy(room)
        _update_room(new_room, student, feature, np.random.randint(room.size + 1, student.wants_max_size - 1))
        assert student_matches.match(student, new_room).cost < student_matches.match(student, room).cost

    elif student.wants_max_size < room.size:
        pref = "wants_max_size"

        # increase max size -> decreased cost
        new_student = copy.copy(student)
        _update_student(new_student, room, pref, np.random.randint(student.wants_max_size + 1, 40))
        assert student_matches.match(new_student, room).cost < student_matches.match(student, room).cost

        # decrease room size -> decreased cost
        new_room = copy.copy(room)
        _update_room(new_room, student, feature, np.random.randint(student.wants_min_size + 1, room.size - 1))
        assert student_matches.match(student, new_room).cost < student_matches.match(student, room).cost

    else:
        # change room size within range -> no change
        new_room = copy.copy(room)
        _update_room(new_room, student, feature, np.random.randint(student.wants_min_size, student.wants_max_size))
        assert student_matches.match(student, new_room).cost == student_matches.match(student, room).cost


@pytest.mark.parametrize("student,room", _get_student_room_combinations())
def test_changing_rank_cost_correct(student, room):
    np.random.seed(hash(student.crsid + room.id) % 2 ** 31)
    student_matches = StudentMatches()

    if student_matches.match(student, room).cost != 0:
        # higher rank -> larger cost magnitude
        if student.rank - 1 > 1:
            new_student = copy.copy(student)
            _update_student(new_student, room, "rank", np.random.randint(1, student.rank - 1))
            assert abs(student_matches.match(new_student, room).cost) > abs(student_matches.match(student, room).cost)

        # lower rank -> smaller cost magnitude
        if student.rank + 1 < Constants.num_in_ballot.value:
            new_student = copy.copy(student)
            _update_student(
                new_student, room, "rank", np.random.randint(student.rank + 1, Constants.num_in_ballot.value)
            )
            assert abs(student_matches.match(new_student, room).cost) < abs(student_matches.match(student, room).cost)

    else:
        # zero cost -> no change with rank
        new_student = copy.copy(student)
        _update_student(new_student, room, "rank", np.random.randint(1, Constants.num_in_ballot.value))
        assert student_matches.match(new_student, room).cost == student_matches.match(student, room).cost
