"""Integration tests for the `src.ballot.shuffle` module."""

from typing import List, Tuple

from src.ballot.shuffle.BlockConfig import BlockConfig
from src.ballot.shuffle.expand import expand_configs
from src.ballot.shuffle.find import find_valid_configs
from tests.integration.utils import read_data


def _get_configs(seeds: List[int]) -> Tuple[List[BlockConfig], List[int]]:
    buildings, room_list, syndicates, student_list = read_data()
    blocks = [block for building in buildings for block in building.blocks]

    return find_valid_configs(syndicates, blocks, seeds, verbose=True)


def _assert_config_equal(config_1: BlockConfig, config_2: BlockConfig):
    assert config_1.seed == config_2.seed
    assert config_1.blocks_by_size == config_2.blocks_by_size
    assert config_1.split_blocks == config_2.split_blocks


def test_find_valid_configs():
    configs, _ = _get_configs(list(range(100)))
    assert len(configs) > 0


def test_expand_configs():
    configs, _ = _get_configs(list(range(100)))

    factor = 4
    expanded = expand_configs(configs, factor, verbose=True)
    assert len(expanded) == len(configs) * factor


def test_regenerate_valid_configs():
    seeds = list(range(100))
    configs, valid_seeds = _get_configs(seeds)

    new_configs, _ = _get_configs(seeds)

    for i in range(len(configs)):
        _assert_config_equal(configs[i], new_configs[i])
