from profilers.utils import get_profile_stats
from src.ballot.shuffle.find import find_valid_configs
from src.run.analyse_in_dtale import analyse_in_dtale
from src.run.run_read import run_read

#%%
blocks, syndicates, room_list, student_list, dfs, pre_allocated_rooms = run_read(mock=True)

#%%
profile_stats_df = get_profile_stats(find_valid_configs, syndicates, blocks, list(range(10000)), verbose=False)

#%%
analyse_in_dtale(profile_stats_df, save=False)
