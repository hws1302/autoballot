import cProfile
import io
import pstats
from pathlib import Path

import pandas as pd


def get_profile_stats(func, *args, **kwargs):
    pr = cProfile.Profile()

    pr.enable()
    func(*args, **kwargs)
    pr.disable()

    s = io.StringIO()
    ps = pstats.Stats(pr, stream=s)

    ps.strip_dirs()
    ps.sort_stats("cumulative")

    return stats_to_df(ps)


def stats_to_df(stats: pstats.Stats):
    # calls includes only primitive calls. Use  to include
    # recursive calls
    df = pd.DataFrame(
        {
            "paths": [Path(func[0]) for func in stats.stats],
            # "lines": [func[1] for func in stats_obj.stats],
            "func": [func[2] for func in stats.stats],
            "calls": [stats.stats[func][0] for func in stats.stats],
            # "rec_calls": [stats.stats[func][1] for func in stats.stats],
            "total": [stats.stats[func][2] for func in stats.stats],
            "cumulative": [stats.stats[func][3] for func in stats.stats],
        }
    )

    df["average"] = df["total"] / df["calls"]
    df["av_cum"] = df["cumulative"] / df["calls"]

    df = df.sort_values("cumulative", ascending=False)

    return df
