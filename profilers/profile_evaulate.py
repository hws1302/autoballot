import shelve

from profilers.utils import get_profile_stats
from src.ballot.evaluate.get_opt_configs import get_opt_configs
from src.ballot.match.StudentMatches import StudentMatches
from src.ballot.match.SyndicateMatches import SyndicateMatches
from src.ballot.shuffle.find import find_valid_configs
from src.run.analyse_in_dtale import analyse_in_dtale
from src.run.run_read import run_read

_SHELF = "data/shelves/profile_evaluate.out"

#%%
blocks, syndicates, room_list, student_list, dfs, pre_allocated_rooms = run_read(mock=True)
student_matches = StudentMatches()
syndicate_matches = SyndicateMatches(student_matches)

#%%
with shelve.open(_SHELF) as shelf:
    try:
        valid_seeds = shelf["valid_seeds"]
    except KeyError:
        valid_seeds = list(range(100000))

valid_configs, valid_seeds = find_valid_configs(syndicates, blocks, valid_seeds, verbose=False)

#%%
with shelve.open(_SHELF, "n") as shelf:
    for key in dir():
        shelf["valid_seeds"] = valid_seeds

#%%
profile_stats_df = get_profile_stats(
    get_opt_configs,
    syndicates,
    valid_configs,
    student_matches,
    syndicate_matches,
    pre_allocated_rooms,
    len(room_list),
    detailed=False,
    verbose=False,
)

#%%
analyse_in_dtale(profile_stats_df, save=False)
